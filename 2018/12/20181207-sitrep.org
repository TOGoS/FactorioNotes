#+TITLE: Terrain Generation Situation Report, 2018-12-07

Biter placement branch is currently at [[https://github.com/wube/Factorio/pull/2204/commits/d81a61af70f762128daa4a0c816526ba77d0c7f4][d81a61af70f762128daa4a0c816526ba77d0c7f4]].

From Twinsen:

#+BEGIN_QUOTE
The noise part looks fine. You should look at how actual entities are
placed. And how that ties to biter expansion.
- Hard bases seem to have just almost only spitter spawners
- The difficulty of a base should be determined mostly by the number
  of worms, but right now harder bases seems to have just spawners,
  with worms spawning typically in the trees(probably due to
  collisions).
- As difficulty increases the bases should proportionally have more
  big worms than small worms. The number of worms should also increase
  with difficulty. The number or spitter spwners should be roughly
  equal to the number of biter spawners(regardless of difficulty). How
  they all mix and how it depends on the "richness" within the noise
  spot(if at all) is up to you, but this should be improved as now it
  even creates square patterns: (edited)
#+END_QUOTE

He included [[https://files.slack.com/files-pri/T11HJ5P7X-FEN8R7E5D/image.png][a screenshot of some overly grid-placed biter nests]].
I think the trouble is that probability goes >= 1,
so a nest is just placed everywhere it can be.

So, to-do about placement:
- [X] Fix griddiness (presumably by clamping probability <1)

*** DONE Probability clamping

What if I do like ~1 - (1 / (1 + probability))~?

| Input |     Output |
|-------+------------|
|  -100 |  1.0101010 |
|    -2 |          2 |
|    -1 |    1 - 1/0 |
|  -0.5 |        -1. |
|     0 |          0 |
|   0.5 | 0.33333333 |
|  0.75 | 0.42857143 |
|     1 |        0.5 |
|     2 | 0.66666667 |
|     3 |       0.75 |
|   100 | 0.99009901 |
#+TBLFM: $2=1 - (1/(1 + $1))

I want large numbers to -> 0
small numbers to -> 1

|    x |        1/x |
|------+------------|
|    0 |        1/0 |
|  0.5 |         2. |
| 0.75 |  1.3333333 |
|    1 |          1 |
|    2 |        0.5 |
|    3 | 0.33333333 |
|    4 |       0.25 |
|    5 |        0.2 |
#+TBLFM: $2=1/$1

[2018-12-12T13:18:27-06:00] In the end I didn't use the curve.  Instead I just clamped at ~0.25 + distance_factor * 0.05~
