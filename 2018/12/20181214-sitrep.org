#+TItLE: Terrain Generation Situation Report, 2018-12-14..2018-12-17

Took care of several of Twinsen's requests [[./20181212-sitrep.org][the other day]], and he's given feedback:

#+BEGIN_QUOTE
#+BEGIN_QUOTE
Do you think starting area should affect biter base placement at all?
#+END_QUOTE
No.

Or the only effect could be that it simply "pushes" everything further
out, so the edge of the starting area is always the same "intensity",
up until maximum+starting area size. But it's probably not worth
bothering.

Unfortunately the increased randomness made the starting area bases way too unpredictable, with impossible to kill bases sometimes spawning too close to you.
Can you make it so the extra randomness only linearly comes into effect based on distance(or intensity?) ? with no effect near the starting area.

Also can you make some changes so the relative number of worms to spawners increases slightly with "intensity" ?
#+END_QUOTE

To which I have replied:

#+BEGIN_QUOTE
Pushing everything out by that much should be straightforward, as should ramping up the randomness near the starting area; I'll try that.
#+END_QUOTE

But first I want to finish the GenerateMapGUI fix that I'm in the middle of.

- [-] Biter fixes (baseline [[https://github.com/Wube/Factorio/commit/e157bbe88634a29eda6a85349bb8a3559c582dcd][e157bbe88634a29eda6a85349bb8a3559c582dcd]])
  - [X] Starting area size should push biter bases out by that much; i.e. add it to distance to first X rather than multiplying by it
    - Existing version:
      #+BEGIN_SRC
        local distance_height_multiplier = noise.max(0, 1 + (noise.var("distance") - distance_unit * distance_factor) * 0.001 * distance_factor)
      #+END_SRC
		|       seed | medium worm distance | big worm distance |
		|------------+----------------------+-------------------|
		|        ??? |                  489 |              1678 |
		|  860428120 |                  490 |              1681 |
		| 4271789055 |                  559 |              1513 |
    - New version:
      #+BEGIN_SRC 
        local distance_height_multiplier = noise.max(0, 1 + (distance_outside_starting_area - distance_unit * distance_factor) * 0.001 * distance_factor)
      #+END_SRC
		|       seed | medium worm distance | big worm distance |
		|         ?? |                  650 |              1717 |
		|  250676640 |                  736 |              1668 |
		| 2063419343 |                  758 |              1692 |
		Eh that seems fine.
  - [X] Have biter base blobbiness start low and ramp up with distance
    - 8f02db4b216f99f1201dfd1f068ee01797082605
  - [ ] Increase worm-to-spawner ratio [more] with distance
- [X] Terrain fixes (based on [[https://github.com/Wube/Factorio/commit/e12b6a7863ae3a0e770809795e3e26369f6f9b1c][e12b6a7863ae3a0e770809795e3e26369f6f9b1c]])
  - [X] Increase elevation bias so that what is currently called '50% water' is the default at 100%.
  - [X] Figure out what Twinsen means about the default elevation selection being broken and fix it
    - It starts out showing NOTHING
    - [X] Default to 0.16 elevation for existing maps
    - [X] Make 'default' mapgen preset specify lakes
    - [X] Make the GenerateMapGUI initialize the drop-down!!!
  - [X] Remove correlation between elevation and temperature because
    Twinsen thinks it emphasizes the fakeness of cliffs
  - [X] Make cliffs on maximum slider settings still less ridiculous
- [ ] Figure out how frequency/size/richness of tiles works and respond to [[https://github.com/wube/Factorio/pull/2223#issuecomment-446581328][Twinsen's question]].

Q: What factorio version should I use for generating map previews for biter placement?

A: [[https://github.com/Wube/Factorio/commit/61cbea486b57b49ef8696d1d0539f809d6ebfedf][61cbea486b57b49ef8696d1d0539f809d6ebfedf]] (merged master with engine updates into biter-placement-fixing) should do.

** Debugging GenerateMapGUI

Why does it start out blank?

I think the problem is that ~propOverrideOptions.second[index]->name~ is actually the spelled out name,
not the internal code.  I need to match by index.

#+CAPTION: GenerateMapGUI
#+BEGIN_SRC C++ -l 1740
    this->terrainGeneratorSelectors[propName].setSelectedIndex(-1);
    const std::string& expressionName = settings.propertyExpressionNames.at(propName);
    for (size_t index = 0; index < propOverrideOptions.second.size(); ++index)
    {
      if (propOverrideOptions.second[index]->name == expressionName)
      {
        this->terrainGeneratorSelectors[propName].setSelectedIndex((int)index);
        break;
      }
    }
#+END_SRC

Lawl actually that was not the problem, or at least not the only one.
I had attempted to put some default settings on the default preset,
but that preset's definition is so short I put it on a different preset.
However, when I put it on the actual default preset I got an error:
"The default preset can't have any settings defined."
(thrown by MapGenPreset.cpp, line 18).

Things to try
- [ ] Comment out the 'default preset can't have any settings defined' rule
  - Maybe the reason that rule is there:
    The "defaults" are initially just blank (i.e. whatever MapGenSettings constructor does)
    and don't get loaded until you click the button.
- [ ] Use a different mechanism for determining default presets,
  such as looking at the actual named noise expressions that have options
  and if there's a default one (e.g. the "elevation" expression for the elevation property)
  that is purely a reference to another, picking that as the default.
  - Advantages:
    - Automatically does what we want
    - Would not require changing how presets work
  - Disadvantages:
    - Less explcit about what a default is
    - DOESN'T ALLOW NEW MAP AND EXISTING MAPS TO DEFAULT DIFFERENTLY
  - I called this ~terrain-generation/generatemapgui-default-generators-to-aliased~ and it's [[https://github.com/Wube/Factorio/commit/039580e5f5f099112ee378ef124ea098dbe6345a][039580e5f5f099112ee378ef124ea098dbe6345a]].
- [ ] Pick the highest-numbered one that's named like 0_17-...
