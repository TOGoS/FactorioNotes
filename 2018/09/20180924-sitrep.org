#+TITLE: Terrain Generation Situation Report, 2018-09-24

*** Crash!

[[http://picture-files.nuke24.net/uri-res/raw/urn:bitprint:ETNIAK2MKJY6VD3LNEYXQIP2HTW3HRDP.RFUFGLOE6MGJOTMKP5G2BXORZPRKPKFD3RXVS4Y/factorio-current.log][when I start a game and start walking]] using cbbaeb78fc13a93cbb456816f74378ebcb40b52c.

This is probably because I don't fix up elevation/cliffiness when deserializing.

And this is why I don't push merges until I've tested them!

Time for bed.

*** Get some of that stuff merged

Merged fixed into spot-noise-ore-placement factorio-git-commit:2312ffcb50742c1b75b37e0001b340a31d7c0a0a

*** Cliff test failures

But oh no a unit test fails:

#+BEGIN_SRC
Failed tests:
[CliffGenerator/InitialPlacementAlignsAtChunkEdges]
 /opt/src/Factorio/tests/TestCliffGenerator.cpp:118: Expected 1 but was 0
#+END_SRC

Let's run the tests for some different versions to see where tests broke.

Is this cliff test failure even reproducable?

Ran tests several times on Windows.  Mostly they passed.

#+BEGIN_SRC
Test selection:
Suite or test contains Cliff
[CliffGenerator/InitialPlacementAlignsAtChunkEdges] 1.6339s
Looking for saves to test in C:\Users\TOGoS\workspace\Wube\Factorio\data\..\tests\TestedSaves
Success: 1 tests passed.
Test time: 1.64 seconds.
#+END_SRC

Until they didn't:

#+BEGIN_SRC
Test selection:
Suite or test contains Cliff
[CliffGenerator/InitialPlacementAlignsAtChunkEdges] Factorio crashed. Generating symbolized stacktrace, please wait ...
c:\users\togos\workspace\wube\factorio\libraries\stackwalker\stackwalker.cpp (907): StackWalker::ShowCallstack
c:\users\togos\workspace\wube\factorio\src\util\logger.cpp (509): Logger::writeStacktrace
c:\users\togos\workspace\wube\factorio\tests\main.cpp (210): TestReporter::ReportFailure
c:\users\togos\workspace\wube\factorio\libraries\unittest++\testresults.cpp (38): UnitTest::TestResults::OnTestFailure
c:\users\togos\workspace\wube\factorio\libraries\unittest++\currenttest.cpp (23): UnitTest::CurrentTest::OnTestFailure
c:\users\togos\workspace\wube\factorio\tests\testcliffgenerator.cpp (118): SuiteCliffGenerator::UnitTest__NullFixtureInitialPlacementAlignsAtChunkEdgesHelper::Run
c:\users\togos\workspace\wube\factorio\libraries\unittest++\test.cpp (45): UnitTest::Test::Run
c:\users\togos\workspace\wube\factorio\libraries\unittest++\testrunner.cpp (69): UnitTest::TestRunner::RunTest
c:\users\togos\workspace\wube\factorio\tests\main.cpp (290): TestRunner::runList
c:\users\togos\workspace\wube\factorio\tests\main.cpp (1279): main
f:\dd\vctools\crt\vcstartup\src\startup\exe_common.inl (79): invoke_main
f:\dd\vctools\crt\vcstartup\src\startup\exe_common.inl (288): __scrt_common_main_seh
f:\dd\vctools\crt\vcstartup\src\startup\exe_common.inl (331): __scrt_common_main
f:\dd\vctools\crt\vcstartup\src\startup\exe_main.cpp (17): mainCRTStartup
ERROR: SymGetLineFromAddr64, GetLastError: 487 (Address: 00007FF877B02784)
00007FF877B02784 (KERNEL32): (filename not available): BaseThreadInitThunk
ERROR: SymGetLineFromAddr64, GetLastError: 487 (Address: 00007FF877EF0C31)
00007FF877EF0C31 (ntdll): (filename not available): RtlUserThreadStart
Stack trace logging done
Factorio crashed. Generating symbolized stacktrace, please wait ...
c:\users\togos\workspace\wube\factorio\libraries\stackwalker\stackwalker.cpp (907): StackWalker::ShowCallstack
c:\users\togos\workspace\wube\factorio\src\util\logger.cpp (509): Logger::writeStacktrace
c:\users\togos\workspace\wube\factorio\tests\main.cpp (223): TestReporter::ReportFailure
c:\users\togos\workspace\wube\factorio\libraries\unittest++\testresults.cpp (38): UnitTest::TestResults::OnTestFailure
c:\users\togos\workspace\wube\factorio\libraries\unittest++\currenttest.cpp (23): UnitTest::CurrentTest::OnTestFailure
c:\users\togos\workspace\wube\factorio\tests\testcliffgenerator.cpp (118): SuiteCliffGenerator::UnitTest__NullFixtureInitialPlacementAlignsAtChunkEdgesHelper::Run
c:\users\togos\workspace\wube\factorio\libraries\unittest++\test.cpp (45): UnitTest::Test::Run
c:\users\togos\workspace\wube\factorio\libraries\unittest++\testrunner.cpp (69): UnitTest::TestRunner::RunTest
c:\users\togos\workspace\wube\factorio\tests\main.cpp (290): TestRunner::runList
c:\users\togos\workspace\wube\factorio\tests\main.cpp (1279): main
f:\dd\vctools\crt\vcstartup\src\startup\exe_common.inl (79): invoke_main
f:\dd\vctools\crt\vcstartup\src\startup\exe_common.inl (288): __scrt_common_main_seh
f:\dd\vctools\crt\vcstartup\src\startup\exe_common.inl (331): __scrt_common_main
f:\dd\vctools\crt\vcstartup\src\startup\exe_main.cpp (17): mainCRTStartup
ERROR: SymGetLineFromAddr64, GetLastError: 487 (Address: 00007FF877B02784)
00007FF877B02784 (KERNEL32): (filename not available): BaseThreadInitThunk
ERROR: SymGetLineFromAddr64, GetLastError: 487 (Address: 00007FF877EF0C31)
00007FF877EF0C31 (ntdll): (filename not available): RtlUserThreadStart
Stack trace logging done
C:\Users\TOGoS\workspace\Wube\Factorio\tests\TestCliffGenerator.cpp(118): error: Failure in InitialPlacementAlignsAtChunkEdges: Expected 1 but was 0
3.1669s
Looking for saves to test in C:\Users\TOGoS\workspace\Wube\Factorio\data\..\tests\TestedSaves
FAILURE: 1 out of 1 tests failed (1 failures).

Failed tests:
[CliffGenerator/InitialPlacementAlignsAtChunkEdges]
 C:\Users\TOGoS\workspace\Wube\Factorio\tests\TestCliffGenerator.cpp:118: Expected 1 but was 0
Test time: 3.17 seconds.
#+END_SRC

Alright, let's run the other 2 versions several times!

| commit                                   | pass count | failure count |
|------------------------------------------+------------+---------------|
| 2d6ba0203c55e7278f5bbcb15843aeb03cb1d74a |         48 |             2 |
| 890aced2ec1bb1dc6e7b3683ef5a671bd9fe7ec2 |         36 |             1 |
| ff9f297ab24182c40b01427ce7a68a0ba60487c4 |          1 |             0 |
| 2312ffcb50742c1b75b37e0001b340a31d7c0a0a |         60 |             5 |

If this turns out to have been a bug in old version I won't worry about it.
But while things are compiling, here's the output from the cliff tests with =--enable-logging=.

#+BEGIN_SRC
   3.718 Info TestCliffGenerator.cpp:107:   500 chunks, 97 interesting east edges, 108 interesting south edges
   3.718 Info TestCliffGenerator.cpp:108:     1 errors on east edges
   3.719 Info TestCliffGenerator.cpp:109:     0 errors on south edges
   3.720 Error TestCliffGenerator.cpp:113: Some cliffs don't match up!
   3.721 Error TestCliffGenerator.cpp:115:   -18054,-1287 east doesn't match -18053,-1287 west (-1 != 0) at cell 3
#+END_SRC

Though it may be useful to LOG_INFO which seed was used for the random generator
so that specific instances of the error can be reproduced and debugged.

Errors from 2d6ba0203c55e7278f5bbcb15843aeb03cb1d74a:

#+BEGIN_SRC
   2.242 Error TestCliffGenerator.cpp:113: Some cliffs don't match up!
   2.242 Error TestCliffGenerator.cpp:115:   16425,11286 east doesn't match 16426,11286 west (1 != 0) at cell 6
#+END_SRC

#+BEGIN_SRC
   2.226 Error TestCliffGenerator.cpp:113: Some cliffs don't match up!
   2.226 Error TestCliffGenerator.cpp:115:   421,-18417 south doesn't match 421,-18416 north (-1 != 0) at cell 1
   2.226 Error TestCliffGenerator.cpp:115:   -11089,-239 east doesn't match -11088,-239 west (0 != -1) at cell 6
#+END_SRC

Going to chalk that up to rounding error for now.

Add to the to-do list for later.

** DONE Appease spell checker?

https://ci.factorio.com/#/builders/15/builds/71 says:

#+BEGIN_SRC
/home/build/buildbot-master/master/workers/local/pr-test-lua/build/scripts/spellcheck/../../data/changelog.txt: Line 171 Spelling error: "AutoplaceSpecifications", suggestions: ['Underspecification', 'Specifications', 'Misspecification', 'Specification', 'Compartmentalization', 'Electroencephalographic', 'AutoplaceSpecification']
/home/build/buildbot-master/master/workers/local/pr-test-lua/build/scripts/spellcheck/../../data/changelog.txt: Line 173 Spelling error: "NamedNoiseExpressions", suggestions: ['Impressionableness', 'Expressionisms', 'Subexpressions', 'Expressionistic', 'Metropolitanization', 'Underrepresentation']
#+END_SRC

Maybe check the changelog style guide...

Oh the words probably just need to be added to =scripts/spellcheck/custom_dictionary.txt=: factorio-git-commit:fdb40509b9e048198ddf70c4b7482550cc7598cd

** DONE [#A] Log seed used for cliff test so that errors can be reproduced more easily

#+BEGIN_SRC
   3.791 Info TestCliffGenerator.cpp:110:   500 chunks, 101 interesting east edges, 102 interesting south edges
   3.792 Info TestCliffGenerator.cpp:111:     1 errors on east edges
   3.792 Info TestCliffGenerator.cpp:112:     0 errors on south edges
   3.793 Error TestCliffGenerator.cpp:116: Some cliffs don't match up!  Random seed: 1537909225
   3.794 Error TestCliffGenerator.cpp:118:   -15944,-11417 east doesn't match -15943,-11417 west (-1 != 0) at cell 5
#+END_SRC

factorio-git-commit:70bc429be4db9b4bb7358a2f7eaa4c238658f3fc




** During fixing, new error

/opt/src/Factorio/tests/TestCompiledMapGenSettings.cpp:29: error: Failure in SerializationRoundTrip: Expected (one thing) but got (a different thing)

Was this introduced by d47db99a45a3fb051613eaf8af24c4d2cadf7dab?
Let's test it and d869ec926a3a1e13d3ccd2aa019059433e4fdec2.

| factorio commit ID                       | test result |
|------------------------------------------+-------------|
| d869ec926a3a1e13d3ccd2aa019059433e4fdec2 | [[urn:sha1:YRKL5IHWHB4RURD62IPVQCH65R6ALDJP][passes]]      |
| d47db99a45a3fb051613eaf8af24c4d2cadf7dab | [[urn:sha1:AQ7OAABL32T4HTVXQJ4METW7MHXTKX2W][fails]]       |
| 48a5c47f2ca80797a8f8aee87613717f4a931059 |             |

So yeah that broke it somehow.

Fixed (?) by removing CRC altogether.  Rebased to factorio-git-commit:48a5c47f2ca80797a8f8aee87613717f4a931059.

Cleared away merges into spot-noise-ore-placement (factorio-git-commit:0c4d31234b16334d5d758f3b1aa8ccb044ed4c79)
and started a relatively 'fresh' merging: factorio-git-commit:f312e6a685e2160811f1fa6339ad60940c198dad.


** Notes on git commits

Occasionally resetting spot-noise-ore-placement to c1b83e5bf2ed2c4d3797a6e84557cd0fa0b6373e before merging
so that I don't clutter this history with a million consecutive merges.

** DONE Fix specific problems Rseding found

- [X] Put POI logic behind a compile-time flag
  - +factorio-git-commit:08c688b6bcc0e30227ac96952237c63db1d9fed2+
  - factorio-git-commit:baefd4ae0dea47d7f1b98d7683101f641917f17f
- [X] Clean up _getAutoplaceFrequencySizeRichness from AutoplaceUtils
  - factorio-git-commit:baefd4ae0dea47d7f1b98d7683101f641917f17f
- [X] Remove whole concept of tile properties debug visualisation DebugVisualizationRenderer.cpp 
  - factorio-git-commit:729d17cf5d3005cd3d1ef5f597d8c2df7f62942b
- [X] Why don't you just return *this->probabilityExpression; ?
  - Because there's no copy constructor and I wanted the copy to be obvious and ugly.
  - But Robert says
    #+BEGIN_QUOTE
    There is a copy constructor but it's marked as explicit. If you
    want to envoke it you have to manually do it via:
    =return PropertyTree(*this->richnessExpression);=
    which will copy the property tree and then return it by moving.
    #+END_QUOTE
  - factorio-git-commit:9d1278d63c6e1e799de2fb10d5b5e5cc0ca09446
- [X] Rename placeeId
  - factorio-git-commit:b72088b3e2a86e74d791ebd3cea31f8b12ce0e53
  - But I'm leaving that off the branch for now because it's silly
- [X] Try to avoid std::map-lookup elevation/cliffiness registers 'at run time'
  - factorio-git-commit:c56c29f0a082554c89553e7fa70c423350f4b9b8
- [X] 2018-09-26 EODish: Generate some previews from
  factorio-git-commit:a7e507bf76dd51b8ce3c85d3a42dd5be910dc5a0 to make
  sure nothing's broken too bad since...
  factorio-git-commit:c1b83e5bf2ed2c4d3797a6e84557cd0fa0b6373e, I
  suppose.
  - Woop woop they're identical.
- [X] change sizeof(this) to sizeof(*this)
  - factorio-git-commit:d47db99a45a3fb051613eaf8af24c4d2cadf7dab
- [X] Don't keep unneeded vector of maxProbabilities during generateBasicTiles
  - factorio-git-commit:4a10f4d36bfa3495bd6e92927ecd2c073c1b0dc3
  - oh no that turned the starting lake green!
  - actually that vector *is* needed.

** TODO Fix specific problems Rseding found

Putting aside the general complaints about =PropertyTree=s and =std::map<std::string>=s for now.

- [ ] 'Clean up' skipping CompiledMapGenSettings
  #+BEGIN_QUOTE
  This saving of the compiled settings is neat for performance but the
  way it's implemented needs to be cleaned up.
  
  What it should be doing is: during saving take the result of the
  StringWriteStream and write it to the standard output as the
  string. Then, when you load here you first load the full string
  using input.load(string); and if you don't want to use it, you just
  don't - and you're done reading from input.
  
  That way the entire input.skip(...) function isn't needed.
  #+END_QUOTE
  
  #+BEGIN_QUOTE
  If [avoiding a skip and matching the string format] is the goal then
  the saving at least needs to save using the standard save method
  since strings don't save a uint32_t for size but a space-optimized
  uint32_t.
  #+END_QUOTE
- [ ] src/Map/BasicTilesMapGenerationTask.cpp : Maybe generateBasicTiles can be consolidated.  Rseding doesn't like the TileID array and the copying.
- [ ] ...
