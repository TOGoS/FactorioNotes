Hmm, looking at maps, there are no ores in the southwest!

- In a2bf0d169a992be543db1b51276f7ebab585db87 (early spot noise commit) it still worked
- 68f7e8e34aaf6f7e43f94377201e76f8627e0b84/a2bf0d had some similar poblem with northeast and southwest - spots are very small
- 1982e55bb25cdd1ce6c64089737db72382a46a80/2932fa does also
- 9eb5afc1bc9e090391647503ff24cf9f59fcb519/130dee (last from Prague) has no ores to southwest

Logged regions and quantities placed from NoiseProcedureOps::SpotNoise::generateSpotList.
Conspicuously, it doesn't seem to get called with negative x and positive y!!!

LMAO.  Check this line in ```NoiseProcedureOps::SpotNoise::getSpotLists```


```
    for (int32_t x = leftTop.y; x < rightBottom.x; ++x)
```

Bro.
