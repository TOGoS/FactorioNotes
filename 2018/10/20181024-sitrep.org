#+TITLE: Terrain Generation Situation Report, 2018-10-24

Twinsen says:

#+BEGIN_QUOTE
All seems good to me. Please push fixes so buildbot tests pass, asap. Then you can double check any leftover TODOs and it can probably get merged to master.

I updated the changelog. Please add anything I forgot, for example some information about the new noise system modders can use. (edited)
You probably saw the new map generator gui where you can change the settings while looking at the preview. An option will also make it update automatically every time you move a slider.
So a nice optimization would be making the preview only regenerate ores layer when only ores were changed. Something to keep in mind for later if there is time left.
#+END_QUOTE

Rseding's still complaining about "placeeID" though.

- [X] Rename 'placee ID' ([[factorio-git-commit:3468382da4f261850c3a1fb8bee44e51921563d3][autoplace-refactoring]])
- [X] Merge master into autoplace-refactoring
- [X] Check Twinsen's changes to changelog
- [X] Fix cliff placement on map-gen-size-whatever
- [X] Merge that fix into spot-noise-ore-placement, test it
- [X] Merge autoplace-refactoring into spot-noise-ore-placement

** Thoughts on fixing cliff graphics

- Remove the west-to-north graphic that causes problems
- Draw south-facing cliffs and corners below ground entities

** Squashing

[[http://tfmpm.togos-fbs.nuke24.net/compare-maps?dataCommitId%5B%5D=9583de785cb7bbe54d1c0d96ff18dc94661e2bcc&dataCommitId%5B%5D=982a72d430aaddddcbef96a205804e34c43a96ad&dataCommitId%5B%5D=ed8aa8f5f4e012f946df52b7fee53de0dc3362cf][Ruh roh]].  I squashed autoplace-refactoring, spot-noise-ore-placement, and the mapgensize changes.
The squashed version (9583de785cb7bbe54d1c0d96ff18dc94661e2bcc)
results in maps that look a bit different than the old ones
(factorio version cdfd4517b525bc317ce018da96478778df05db06
data version 982a72d430aaddddcbef96a205804e34c43a96ad).

Maybe the mapgensize changes messed it up?

Yes.  When I use factorio version
982a72d430aaddddcbef96a205804e34c43a96ad to generate map previews they
look like the 'wrong' 9583de785cb7bbe54d1c0d96ff18dc94661e2bcc ones.

For good measure I'll also test the parent of the commit where I
introduced the mapgensize change on the squashy branch,
e030c0997d4990b01086ecd06eb1a30aba358242.  Yeah it's fine.

So I pushed the version of the branch just before the mapgensize changes to github.
I'll try to figure it out tomorrow.

Also, watch out for this:

#+BEGIN_SRC
[NoiseProgram/ConsistencyOfDifferentNoiseImplementationsRandomCases] Assertion failed: (scratch.getSize() >= gridWidth), function noise, file /Users/build/buildbot-worker/worker/pr-test-mac/build/src/Noise/Noise.cpp, line 353.
#+END_SRC
