#+TITLE: Terrain Generation Situation Report, 2018-10-30

#+BEGIN_QUOTE
I was about to merge but i realized the map version migrations are not correct. So please update them and then merge to master.
I can see some MapVersion(0, 17, 0, 71) in CompiledMapGenSettings
and a bunch of them in MapGenSettings.
They all need to be changed to the same number (whatever the current master is +1)

This is why I use 0, 17, 0, 999) (or some other random number) so when the merge comes it's just about doing one replace all.
Now you have to go through all your changes and make absolutely sure you don't miss any migration.
#+END_QUOTE
-- Twinsen

Okay so how was it supposed to look?

#+BEGIN_SRC
C:\Users\TOGoS\workspace\Wube\Factorio>grep -R "(0, 17, 0, 1001)" src
src/Map/CompiledMapGenSettings.cpp:  if (input.mapVersion < MapVersion(0, 17, 0, 1001))

C:\Users\TOGoS\workspace\Wube\Factorio>grep -R "(0, 17, 0, 1002)" src
src/Map/Map.cpp:const MapVersion Map::outputVersion(0, 17, 0, 1002); // 1002 = mapgensize numberfication; 1001 = autoplace-refactoring
src/Map/MapGenSize.cpp:  if (input.mapVersion < MapVersion(0, 17, 0, 1002))
#+END_SRC

So the 2 versions are referenced in one place each other than outputVersion.

cb27998ed3be44f802cea4d8b452fb7f423f45d1 rewrites the version numbers to 998 and 999 so that Twinsen knows what to fix up.

#+BEGIN_QUOTE
I was hoping you would merge it now
#+END_QUOTE
-- Twinsen

#+BEGIN_QUOTE
oh, sorry, I missed that part
in that case I'll go ahead and do it my ideal way
(which will take a while because it requires rewriting history and then checking to make sure I did it right a few times)
#+END_QUOTE
-- Me

*** TODO Merge [3/6]

- [X] Check out master
- [X] Merge autoplace-refactoring
  - [X] Cherry-pick ab4f9f7b83c82511c000497a194072e06ed25591
  - [X] fix MapVersions to whatever the next one is (0, 17, 0, 76)
    - daa521b8fb2d52e6f3ea4c627b8a79eacf0582c0
  - [X] Push to togos-fbs, run tests, generate map(s)
  - [X] Play locally
  - [X] Do f53715c56adc9eab02b7bc0d3b80ea2409a4acf9
  - [X] Rebase to squash that into autoplace-refactoring
    - bc79883adc5aa2dd2d7a6ba0e3f903ce296e9edc
  - Run tests in Debug mode?
  - [X] Push to github!
  - [X] Make sure buildbot says the tests pass
- [-] Cherry-pick 60408d880dba2e78809fc50229661804f4df4906
  - [X] Test locally
  - [ ] Push to togos-fbs, run tests, generate map(s)
  - [X] Push to github!
  - [ ] Make sure buildbot says the tests pass
- [X] Cherry-pick 692072de37026ca699ec0c67a48fa785a4bed8df
  - [X] Fix MapVersion (to 77)
- [-] Cherry-pick twinsen's fix and my fix to it
  - [X] Cherry-pick 93d22ea5d26df33f8c10cf5ac769eb1569bfb14b
  - [X] Fix his description 
    - 98e5afaebb56072a44de40819dcb47e89458cd9e
  - [ ] TFMPM-run tests and generate maps
  - [ ] Test game in debug mode
  - [ ] Push to github
- [ ] Celebrate while biking to work

Looks like std::log2 [[https://github.com/wube/Factorio/pull/2153/commits/3e66225ab7a759f44c7442a6ce2b24297ff9045d][doesn't work exactly the same everywhere]], so we might have to add our own ~Math::log2Precise~.
Posilla might already be on it.

#+BEGIN_QUOTE
I wouldn't merge it until std::log2 is replaced. How about using this? http://git.musl-libc.org/cgit/musl/tree/src/math/log2.c
#+END_QUOTE
-- Posilla

Also, tests are slow due to all the expression compiling

#+BEGIN_SRC
Virtually all of the time (89%) is spent inside SHA1WriteStream inside NoiseExpressions::calculateExpressionID
#+END_SRC
-- Rseding

*** This fastenating thing

Let's compare how long the tests take to run on some different commits:

| description                   | commit ID                                | result | duration |
|-------------------------------+------------------------------------------+--------+----------|
| pre-autoplace-refactoring     | 462b9ad0dc1a712dbc761280129021e215bc1b75 | passes |   104.67 |
| autoplace refactoring+etc     | 98e5afaebb56072a44de40819dcb47e89458cd9e | passes |   583.33 |
| cached CompiledMapGenSettings | 1304d2ae18e87567a808eaae8976801e04b20dc9 | passes |   121.32 |
| deferred                      | d3932e790ed1fd15a466b9477e26f5f8560c9819 | passes |   124.59 |

So yeah, that seems to help.

Rebased to 28916c758213d1558c7435939337728fcd7126c9.  [[https://github.com/wube/Factorio/tree/terrain-generation/compiled-map-gen-settings-cache][Pushed to GitHub]] and made [[https://github.com/wube/Factorio/pull/2155][a pull request]].

Did some other minor clean-ups.

201fe7a256209c5551d414cdce66d29802d37110 switches to deferred compilation of mapgensettings,
the thinking being that some mapgensettings won't need to be compiled at all.
It didn't make it faster though.  So I'll leave that one out.
