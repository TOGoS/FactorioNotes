#+TITLE: 2018-10-12

Twinsen made some trello cards.

https://trello.com/c/2kwFki9x/327-map-generator-resource-generation-and-programmable-noise

** Remove biter nest decoration

Okay they want me to remove the enemy base decoration.
I reverted 0e392f74180c812915c4d88bb7b531d4dcd9271f.
There's still lichen, though.  So okay where'd that come from.

#+CAPTION: git blame "0e392f74180^" data/base/prototypes/decorative/decoratives.lua
#+BEGIN_SRC
bbdc0fee5a2 data/base/prototypes/decorative/decoratives.lua (Ernestas        2018-07-02 17:18:45 +0200  4840)     name = "lichen",
bbdc0fee5a2 data/base/prototypes/decorative/decoratives.lua (Ernestas        2018-07-02 17:18:45 +0200  4841)     type = "optimized-decorative",
bbdc0fee5a2 data/base/prototypes/decorative/decoratives.lua (Ernestas        2018-07-02 17:18:45 +0200  4842)     subgroup = "grass",
bbdc0fee5a2 data/base/prototypes/decorative/decoratives.lua (Ernestas        2018-07-02 17:18:45 +0200  4843)     order = "b[decorative]-j[bush]-a[mini]-a[green]",
bbdc0fee5a2 data/base/prototypes/decorative/decoratives.lua (Ernestas        2018-07-02 17:18:45 +0200  4844)     collision_box = {{-0.5, -0.5}, {0.5, 0.5}},
bbdc0fee5a2 data/base/prototypes/decorative/decoratives.lua (Ernestas        2018-07-02 17:18:45 +0200  4845)     selection_box = {{-0.5, -0.5}, {0.5, 0.5}},
bbdc0fee5a2 data/base/prototypes/decorative/decoratives.lua (Ernestas        2018-07-02 17:18:45 +0200  4846)     selectable_in_game = false,
cc6d78c65e5 data/base/prototypes/decorative/decoratives.lua (TOGoS           2018-07-18 03:17:20 +0200  4847)     autoplace =
5fcf2af6efd data/base/prototypes/decorative/decoratives.lua (TOGoS           2018-07-17 12:15:26 +0200  4848)     {
cc6d78c65e5 data/base/prototypes/decorative/decoratives.lua (TOGoS           2018-07-18 03:17:20 +0200  4849)       order = "a[doodad]-e[garballo]",
cc6d78c65e5 data/base/prototypes/decorative/decoratives.lua (TOGoS           2018-07-18 03:17:20 +0200  4850)       sharpness = enemy_autoplace.sharpness * 0.5,
cc6d78c65e5 data/base/prototypes/decorative/decoratives.lua (TOGoS           2018-07-18 03:17:20 +0200  4851)       richness_mutliplier = 1,
5fcf2af6efd data/base/prototypes/decorative/decoratives.lua (TOGoS           2018-07-17 12:15:26 +0200  4852)       richness_base = 0,
cc6d78c65e5 data/base/prototypes/decorative/decoratives.lua (TOGoS           2018-07-18 03:17:20 +0200  4853)       max_probability = 1,
5fcf2af6efd data/base/prototypes/decorative/decoratives.lua (TOGoS           2018-07-17 12:15:26 +0200  4854)       random_probability_penalty = 0.05,
cc6d78c65e5 data/base/prototypes/decorative/decoratives.lua (TOGoS           2018-07-18 03:17:20 +0200  4855)       control = enemy_autoplace.control_name,
cc6d78c65e5 data/base/prototypes/decorative/decoratives.lua (TOGoS           2018-07-18 03:17:20 +0200  4856)       peaks = enemy_autoplace.enemy_autoplace_peaks{}
5fcf2af6efd data/base/prototypes/decorative/decoratives.lua (TOGoS           2018-07-17 12:15:26 +0200  4857)     },
#+END_SRC

Okay I think I [[factorio-git-commit:85fc138399fca49da79f7f376a07b7ca38ae15d1][got that done]].

** Fix the round patches out in the boonies

*** First attempt

This should just be a matter of clamping patch 'size' and then increasing the richness multiplier
after the point where noise amplitude stops increasing.

Let's generate some maps with the current code.

- factorio commit: [[factorio-git-commit:cd175979d2f993ccb318fb9625b4d92d9af584e0]]
- data commit v000: [[factorio-git-commit:85fc138399fca49da79f7f376a07b7ca38ae15d1]]

#+BEGIN_SRC
CMake Error at cmake/compiler.cmake:3 (message):
  GCC version must be at least 8!
#+END_SRC

Okay, guess I'm not using that version for map generating.

Let's try the one just before master was merged into autoplace-refactoring: [[factorio-git-commit:c10e2ba4cf7e2dede0f0889081dd1e3837fdc7de][c10e2ba4cf7e2dede0f0889081dd1e3837fdc7de]]

That one doesn't work because some gui style thing changed.

My Windows build crashes with this bullshit:

#+BEGIN_SRC
c:\users\togos\workspace\wube\factorio\libraries\stackwalker\stackwalker.cpp (907): StackWalker::ShowCallstack
c:\users\togos\workspace\wube\factorio\src\util\logger.cpp (405): Logger::writeStacktrace
c:\users\togos\workspace\wube\factorio\src\util\crashhandler.cpp (169): CrashHandler::writeStackTrace
c:\users\togos\workspace\wube\factorio\src\util\crashhandler.cpp (524): CrashHandler::SignalHandler
minkernel\crts\ucrt\src\appcrt\misc\exception_filter.cpp (219): _seh_filter_exe
f:\dd\vctools\crt\vcstartup\src\startup\exe_common.inl (304): `__scrt_common_main_seh'::`1'::filt$0
ERROR: SymGetLineFromAddr64, GetLastError: 487 (Address: 00007FF6992F6FC0)
00007FF6992F6FC0 (factorio-run): (filename not available): __C_specific_handler
ERROR: SymGetLineFromAddr64, GetLastError: 487 (Address: 00007FFE184B9FDD)
00007FFE184B9FDD (ntdll): (filename not available): _chkstk
ERROR: SymGetLineFromAddr64, GetLastError: 487 (Address: 00007FFE184451C8)
00007FFE184451C8 (ntdll): (filename not available): RtlLookupFunctionEntry
ERROR: SymGetLineFromAddr64, GetLastError: 487 (Address: 00007FFE184B905E)
00007FFE184B905E (ntdll): (filename not available): KiUserExceptionDispatcher
c:\users\togos\workspace\wube\factorio\src\gui\fontbank.cpp (49): FontBank::getFont
c:\users\togos\workspace\wube\factorio\src\entity\speechbubbleprototype.cpp (37): SpeechBubblePrototype::load
c:\users\togos\workspace\wube\factorio\src\id\prototypelist.cpp (41): PrototypeList<EntityPrototype>::executeLoader
c:\users\togos\workspace\wube\factorio\src\data\prototypeloader.cpp (201): PrototypeLoader::loadPrototypeForType
c:\users\togos\workspace\wube\factorio\src\data\prototypeloader.cpp (59): PrototypeLoader::loadPrototypes
c:\users\togos\workspace\wube\factorio\src\data\modmanager.cpp (624): ModManager::loadModPrototypes
c:\users\togos\workspace\wube\factorio\src\data\modmanager.cpp (945): ModManager::processMods
c:\users\togos\workspace\wube\factorio\src\data\modmanager.cpp (424): ModManager::loadData
c:\users\togos\workspace\wube\factorio\src\globalcontext.cpp (497): GlobalContext::init
c:\users\togos\workspace\wube\factorio\src\main.cpp (908): wmain
#+END_SRC

Maybe I need to make a temporary branch to do this work on based off of ada9f9f34cbafc5e4f13bf750f323f9c5762735e
(code commit c10e2ba4cf7e2dede0f0889081dd1e3837fdc7de should work)
(spot-noise-ore-placement before the merge from master and fixes were merged into it)

*** Rebased onto an older commit that works better

Branch: ~terrain-generation/20181012-spot-noise-ore-placement~.

The data commits I'll compare are
- v000: ada9f9f34cbafc5e4f13bf750f323f9c5762735e ; base
- v001: 143103c05c670bd305801137045f5736eb044057 ; reduce circular ore patches

#+BEGIN_SRC bash
util/generate-multiple-map-previews --factorio-commit-id=c10e2ba4cf7e2dede0f0889081dd1e3837fdc7de --data-commit-id="ada9f9f34cbafc5e4f13bf750f323f9c5762735e 143103c05c670bd305801137045f5736eb044057" --scale="1 2 4" --seed=5001
#+END_SRC

**** Results

factorio-git-commit:143103c05c670bd305801137045f5736eb044057 does successfully cap spot sizes
at long range, but also the total quantity at said long range is much higher than I would expect.
It could very well be that I screwed up the calculation for how to compensate for capped size
with richness, so let's revisit that.

*** Richness

Overall quantity, taking richness into account, should increase linearly with distance, from 1x at dist=0
(either from the starting point or from the edge of the starting area) to 2x at ~double_density_distance~ ('ddd').

Charted it out on paper.

richness multiplier past ~spot_enlargement_maximum_distance~ ('semd') should be equal to...

\[ 1 + \frac{1}{1 + \frac{semd}{ddd}} * (distance - semd) \]

(my first LaTeX math formula).

**** Fix attempt #1

- factorio-git-commit:3ab00066a486c9dd0e88570fb9f9bead2e70946c

It is all wrong.  Resource quantities at scale=4 are too high by over 100x.  Oops!
Is my formula wrong?  Did I input it wrong?  Did I forget to divide by ddd somewhere?
Yes I think that's it.  The 'slope' I calculated was actually \( \frac{\Delta richness}{\Delta x/ddd} \).
So I need to multiply the bottom part by \( ddd \):

\[ 1 + \frac{1}{ddd + semd} * (distance - semd) \]

Which simplified to the somewhat nicer:

\[ 1 + \frac{distance - semd}{ddd + semd} \]

Actually can I simplify that more?

\[ \frac{ddd + semd + distance - semd}{ddd + semd} \]
\[ \frac{ddd + distance}{ddd + semd} \]

I think that's less intuitive than the \( 1 + ... \) form,
but I guess I should use it in the code to make the resulting noise program simpler.

**** Fix attempt #2

- [[factorio-git-commit:24a52aa284a45c51dd954b89c87d9e75d815d35f]]

This [[http://tfmpm.togos-fbs.nuke24.net/compare-maps?factorioCommitId%5B%5D=c10e2ba4cf7e2dede0f0889081dd1e3837fdc7de&dataCommitId%5B%5D=24a52aa284a45c51dd954b89c87d9e75d815d35f&dataCommitId%5B%5D=ada9f9f34cbafc5e4f13bf750f323f9c5762735e][seems to have worked better]].
