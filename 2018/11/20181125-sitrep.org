#+TITLE: Terrain Generation Situation Report, 2018-11-25..2018-11-27
** Biter placement fix

Still tryna figure out bite/spitter spaner/worm placement.
Time to break out the bug guns: NoiseDebug!

First I need to find a coordinate to inspect.
Using seed 5001.

Looks like there's a big worm ar about 69,-73?
Maybe it's 68,-72.

*** v010 / c4f6d69329f0aeeed9b7a03684309b9913e46118

Anyway, here's what I find:

#+BEGIN_SRC
  74.794 Info EntityMapGenerationTask.cpp:351: Entity placement within order b[enemy]-a[spawner]: Probability,richness of spitter-spawner: 0.066967,1.000000
  74.794 Info EntityMapGenerationTask.cpp:401: Entity placement within order b[enemy]-a[spawner]: Possibly placing biter-spawner with probability 0.114076
  74.794 Info EntityMapGenerationTask.cpp:420: Entity placement within order b[enemy]-a[spawner]: Not placing biter-spawner
  74.794 Info EntityMapGenerationTask.cpp:351: Entity placement within order b[enemy]-b[worm]: Probability,richness of big-worm-turret: 0.066967,1.000000
  74.795 Info EntityMapGenerationTask.cpp:373: Entity placement within order b[enemy]-b[worm]: big-worm-turret is initial prototype prob,richness: 0.066967,1.000000
  74.795 Info EntityMapGenerationTask.cpp:351: Entity placement within order b[enemy]-b[worm]: Probability,richness of medium-worm-turret: 0.066967,1.000000
  74.795 Info EntityMapGenerationTask.cpp:351: Entity placement within order b[enemy]-b[worm]: Probability,richness of small-worm-turret: 0.066967,1.000000
  74.795 Info EntityMapGenerationTask.cpp:401: Entity placement within order b[enemy]-b[worm]: Possibly placing big-worm-turret with probability 0.066967
  74.795 Info EntityMapGenerationTask.cpp:420: Entity placement within order b[enemy]-b[worm]: Not placing big-worm-turret
#+END_SRC

Neither got placed in this case, probably because I picked the wrong cell to look at.  But things to notice:

- All worms have the same order (~b[enemy]-b[worm]~) and all spawners have the same order (~b[enemy]-a[spawner]~).
  I was confused because I was looking at the order of the prototype, not of its autoplace!
- The probability within the order seems to be the same!

So placement is actually simpler than I was thinking.
Time to figure out what's up with my expressions.

#+CAPTION: demo-enemy-autoplace-utils.lua
#+BEGIN_SRC Lua -l 250
local function enemy_worm_autoplace(distance)
  return enemy_autoplace{
    distance_Factor = distance,
    order = "b[enemy]-b[worm]",
    is_turret = true,
  }
end
#+END_SRC

~distance_Factor~

Well that could be one problem.

*** v011 / e4d4bc533d48dcb60b41a762397b81f075fd953e

This fixed the misspelling of ~distance_factor~ as ~distance_Factor~.

Oh no there's big worms everywhere!  Let's check the log.

#+BEGIN_SRC
   0.685 Script @__base__/prototypes/entity/demo-enemy-autoplace-utils.lua:227: Probability expression for b[enemy]-b[worm]#0:
   0.686 Script @__base__/prototypes/entity/demo-enemy-autoplace-utils.lua:228: enemy_base_probability * (1 + (distance - 0) * 0.001 * 0)
   0.689 Script @__base__/prototypes/entity/demo-enemy-autoplace-utils.lua:227: Probability expression for b[enemy]-a[spawner]#0:
   0.689 Script @__base__/prototypes/entity/demo-enemy-autoplace-utils.lua:228: enemy_base_probability * (1 + (distance - 0) * 0.001 * 0)
   0.779 Script @__base__/prototypes/entity/demo-enemy-autoplace-utils.lua:227: Probability expression for b[enemy]-b[worm]#2:
   0.780 Script @__base__/prototypes/entity/demo-enemy-autoplace-utils.lua:228: enemy_base_probability * (1 + (distance - 1024) * 0.001 * 2)
   0.781 Script @__base__/prototypes/entity/demo-enemy-autoplace-utils.lua:227: Probability expression for b[enemy]-b[worm]#5:
   0.781 Script @__base__/prototypes/entity/demo-enemy-autoplace-utils.lua:228: enemy_base_probability * (1 + (distance - 2560) * 0.001 * 5)
   0.784 Script @__base__/prototypes/entity/demo-enemy-autoplace-utils.lua:227: Probability expression for b[enemy]-a[spawner]#1:
   0.784 Script @__base__/prototypes/entity/demo-enemy-autoplace-utils.lua:228: enemy_base_probability * (1 + (distance - 512) * 0.001 * 1)
#+END_SRC

Yeah, makes sense.  ~1 + (distance - large_number)~ is going to go below zero and when multiplied by a negative number, become positive.
Let's clamp that.

*** v012 / e6cc579f5a5aca9ef49180871bb0701ca67f2c2f

distance_height_multiplier for biter placement clamped to >= 0.

This mostly works.  However!  There are some things I don't like:

- Maybe spitter spawners and medium worms start too far away?
- When spitter spawners show up, biter spawners completely disappear.

I could add some ~random_probability_penalty~ so that things with slightly higher probability don't necessarily replace those with lower.

*** v012

That seems to do the job.

[[https://github.com/wube/Factorio/pull/2204/commits/d7fc9db0c3e4a93800b04f85d3bf75c1a66da127][Pushed to github]], made [[https://github.com/wube/Factorio/pull/2204][a pull request]].
But it has conflicts and test failures.

*** Undefined named noise expression 'control-setting:enemy-base:size:multiplier'

#+BEGIN_SRC
C:\cygwin64\home\build\buildbot-worker\worker\pr-test-win\build\tests\TestScenarioHolder.cpp(827): error: Failure in ChangeMapGenSettingsRuntime: Expected that Cannot execute command. Error: Undefined named noise expression 'control-setting:enemy-base:size:multiplier'
#+END_SRC

After too many hours of going "durrrr" I realized that the problem
was that CompiledMapGenSettings wasn't creating ~autoplace-control:...~ constants
for all AutoplaceControl prototypes.

Fixed by dd04343a0f63697748176d0f95b6c6722f453dc3.

First green oval from Buildbot seems to agree that it is fixed.

*** Resolve conflicts

Probablye due to Rseding fixing my bad line endings, there's a conflict in NoiseProgram.cpp.

I'll rebase onto the latest master.

Ooh, he made things ~InputFromPTreeException~.  That's nice I guess.  Still not sure why Git thought that was a conflict.

New commit is 1292ed09169d97aec7b8485a5facec92c4cd9275.

Pushed.

*** Richness

How about if I just ~richness = false~?
Richness seems redundant, given that one can already control ~size~ and ~starting_area_radius~.

I just turned it off.

*** Rebase a few more times

[2018-11-26T18:28:02-06:00]

Rebasing [[https://github.com/wube/Factorio/commit/45d7f8ee5c73a33d1746f8eab0faebdf7ba69b87][45d7f8ee5c73a33d1746f8eab0faebdf7ba69b87]] to
[[https://github.com/wube/Factorio/commit/be47b25955b2de9f0538c4aebe90026575817c98][be47b25955b2de9f0538c4aebe90026575817c98]] in order to put all C++
changes (except for [[https://github.com/wube/Factorio/commit/2607dc6cd57a9f3c98be17cd7127993620e462b0][2607dc6cd57a9f3c98be17cd7127993620e462b0]], which
should immediately be cherry-picked to master because it fixes a bug
from July) before data ones.

Actually I messed that up.  Oh well, now I'll

- [X] check out master
- [X] [[https://github.com/wube/Factorio/commit/68974610342265180b32d2aaaf5a58ba334aa3f9][cherry-pick that fix]]
- [X] push it
- [X] [[https://github.com/wube/Factorio/commit/cad670f93480cd26d921dddf002c60569ea8d60f][rebase biter stuff onto the latest master]]
- [X] push that to github, togos-fbs, etc

OH HECK actually [[https://github.com/wube/Factorio/commit/d4f3d67e2a7d2643f4e180617ba420b17461ff42][d4f3d67e2a7d2643f4e180617ba420b17461ff42]] should probably get cherry-picked to master, too.
And 685bbcfcc7909ef6e4595c71e5500c85ff968a93.  And 195d4def56d9a2d9b56aaa5debd7a7e7943a61ff.

- [X] [[https://github.com/wube/Factorio/commit/73cbdb89cce8365ad4f03c4ab0fab5ed506fdaea][cherry-pick all that to master]] and push to GitHub
- [X] [[https://github.com/wube/Factorio/commit/ab2bab5f41c882b88ad49bbf90a3c1cffca363e5][Rebase biter stuff again]]
- [X] generate some map previews
  - ~util/generate-multiple-map-previews --factorio-commit-id=82f7ebb4914e2e58482829718cd6943f6e72923d --data-commit-id="82f7ebb4914e2e58482829718cd6943f6e72923d ab2bab5f41c882b88ad49bbf90a3c1cffca363e5"~

*** Compare maps

- Old generator has far more biters bases; more frequent and larger, especially beyond about 1200
- With new generator, size seems to actually *decrease* with distance, down to basically zero outside distance=1024
  - Given the current enemy_base_radius and enemy_base_frequency expressions, they should be pretty much constant size everywhere
  - In game, there are still bases out at -2800.  Here's one at -3992,1160.  Just southwest of a coal patch that *does* show up.
    So why does the map not show them out here?
- If I turn up size with map gen settings, it's [[http://tfmpm.togos-fbs.nuke24.net/uri-res/raw/urn:sha1:AEBHXSIRHDD26GXCU5SUHMNX5SADILIM/8d70e69d1e3b5a44b523d019369b9205f0158731.png][easier to see]] the fadeout.  Radius doesn't decrease, but it looks like they just become improbable.
  Maybe it's because the preview generator doesn't respect ~order~ the same way the in-game generator does?
  Whelp, time to fix that, probably.
- Maybe 290a2f0fe4951b5eedc66bbbba1a96c6429ee13c fixes it?  In which case it should be merged to master.
- I think it does.

*** Oh no command-line map previews are kinda wrong

Uh oh.  For some reason adding mapgensettings seems to screw up placement of other things?
Like even though uranium is normal/normal/normal, it looks different between no map gen settings and using [[http://tfmpm.togos-fbs.nuke24.net/uri-res/raw/urn:sha1:DYHTE7JP7ZW364MZVC5O2CEVZEZ6LHAF][normal.json]].
Sometime I should figure that out.

#+BEGIN_SRC
file:map-gen-settings/normal.json       urn:bitprint:DYHTE7JP7ZW364MZVC5O2CEVZEZ6LHAF.C2REGFBBF556RR6PHQ6NMZTCZZCYVYUUBZMBX2Q
file:map-gen-settings/empty.json        urn:bitprint:BNLNIDAGGCTUVPWFHGHADRWNQMTD73O4.VZWHVU6OKDXFXVAXI3CW7BVOTSHK4SS5GMLMQ3I
#+END_SRC

Well those look the same.

My fix to respect order in map preview generation sure messed things up, though.
Maybe 77023d6b78d7e4d8d5fd6556f3159afc9278a251 fixes it?

Oh no, ~generate-multiple-map-previews~, when given multiple ~--factorio-commit-id~s, treats some of them as data commit IDs.
This might explain some of my map generation confusion from earlier.

77023d6b78d7e4d8d5fd6556f3159afc9278a251 shows all the ores, but still no trees or biters.
Probably because those things get placed with low probability.
We should only replace those with things of a higher 'order' if those higher-order things have high probability.
So let's multiply that ~rand()~ by probability.

Oooh that may have fixed it.

c8155831107a49ddbde10c446b813303fb6248b9

And it looks like it fixes mapping of spot-noise bases, too, which is the whole reason I did that.

*** So back to tweaking

Playtesting (world 104) using [[https://github.com/Wube/Factorio/commit/1f9a6ce430428e7b9267a4f40a811b8c6a2446a4][1f9a6ce430428e7b9267a4f40a811b8c6a2446a4]].
