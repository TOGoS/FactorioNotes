So I got the new stuff running in Docker so that I can run it and figure out what's up with ore generation!

0.16 Dockerization commits: 76660e34a1f531d8ae7790d7995e090dbcc519dc..ee742103cdbab2134436a063e1d2af1a86f1ce7a
(May have to roll back to boost 1.61 for 0.15.x)

0.16.x ('broken'): factorio-43abf2c930ce2a0178e149d34d40842e50a7d211
0.15.x           :



Better long-term solution to starting area ore (and lake) generation:
- Generate a bunch of ores and lakes around *every* starting point in TilePropertiesProvider

