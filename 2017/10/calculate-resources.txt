function total_resource_in(size, resource_name)
  total = 0
  dist = size / 2
  for k, entity in pairs (game.player.surface.find_entities_filtered{name=resource_name, area={{-dist,-dist},{dist,dist}}}) do
    total = total + entity.amount
  end
  return total
end

local resource_names = {'coal','copper-ore','iron-ore','stone','uranium-ore','crude-oil'}
local csv_sep = ';'

function format_resources_in(size)
  return table.concat({size,
    total_resource_in(size, 'coal'),
    total_resource_in(size, 'copper-ore'),
    total_resource_in(size, 'iron-ore'),
    total_resource_in(size, 'stone'),
    total_resource_in(size, 'uranium-ore'),
    total_resource_in(size, 'crude-oil')
  }, csv_sep)
end

function format_resources()
  return table.concat({
    'width' .. csv_sep .. table.concat(resource_names, csv_sep),
    format_resources_in(128),
    format_resources_in(256),
    format_resources_in(512),
    format_resources_in(1024),
    format_resources_in(2048)
  }, "\n")
end

function dump_resource_csv()
  local text = "Resources for " .. game.player.surface.map_gen_settings.seed .. "\n" .. format_resources()
  log("\n" .. text)
  game.print(text)
end

local max_radius = 2048
game.player.force.chart(game.player.surface, {{-max_radius,-max_radius},{max_radius,max_radius}})

dump_resource_csv()

