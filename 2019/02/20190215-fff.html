<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<title>019-02-15 FFF : Terrain Generation Updates</title>
<style>
body {
  background: black;
  color: silver;
  width: 960px;
  margin: auto;
  font-family: sans-serif;
}
h2, h3 {
  color: white;
}
a {
  color: orange;
}
figure {
  margin: auto;
  text-align: center;

}
figcaption {
  font-style: italic;
}
</style>
</head>
<body>

<p>Everyone has different opinions about what makes a good Factorio world.
I've been working on several changes for 0.17, but the overarching theme
has been to make the map generator options screen more intuitive
and more powerful.</p>

<h3>Biter Bases</h3>

<p>In 0.16, the size control for biter bases didn't have much effect.
The frequency control changed the frequency, but that also decreased the size of bases,
which <a href="https://forums.factorio.com/viewtopic.php?t=55113">wasn't generally what people wanted</a>.</p>

<p>For 0.17 we've reworked biter placement using a system similar to that with which
<a href="https://www.factorio.com/blog/post/fff-258">we got resource placement under control</a>.
The size and frequency controls now act more like most people would expect,
with frequency increasing the number of bases, and size changing the
size of each base.</p>

<figure>
<img width="900" height="716" src="http://picture-files.nuke24.net/uri-res/raw/urn:bitprint:OMIU62FYXZUSI6EUSHMLSO5DGDXYTTJL.HMOFTRGI7ZHRZF42NYHA5TZ254OAJ23Z2TWEOYA/0.17-enemy-base-controls.gif"/>
<figcaption>New preview UI showing the effects of enemy base controls.
In reality the preview takes a couple seconds to regenerate after every change,
but the regeneration part is skipped in this animation to clearly show the effects of the controls.</figcaption>
</figure>

<p>And if you don't like the relatively uniform-across-the-world placement of biters,
there are changes under the hood to make it easier for modders to do something different.
Placement is now based on <a href="https://wiki.factorio.com/Prototype/NamedNoiseExpression">NamedNoiseExpressions</a> "enemy_base_frequency" and "enemy_base_radius", which in turn reference "enemy_base_intensity".
By overriding any of those, a modder could easily create a map where biters are found only at high elevations,
or only near water, or correlate enemy placement with that of resources, or any other thing
that can be expressed as a function of location.</p>

<h3>Cliffs</h3>

<!-- Commented out because 0.17 hasn't actually changed the cliff-circle problem that much :P
<p>In 0.16, cliffs have a tendancy to be placed in a circle around starting points.
This happens because there's a dip in the elevation function to force there to be
a lake there, but if all the land around it is high, there will be cliffs
in that transition.</p>

<p>Part of the solution to this was to make ore placement smarter,
not requiring the plateau around the starting are to ensure that they weren't placed underwater.
The other part was simply to add more octaves of noise, so that the starting lake valley
looks more natural, and less like a circle.</p>
-->

<p>We've added a 'continuity' control for cliffs.  If you really
like mazes of cliffs, set it to high to reduce the number of gaps in cliff faces.
Or you can turn it way down to make cliffs very rare or be completely absent.</p>

<figure>
<img src="https://eu1.factorio.com/assets/img/blog/fff-282-cliff-controls.gif" width="900" height="664"/>
<figcaption>Changing cliff frequency and continuity.  Since cliffs are based on elevation,
you'll have to turn frequency <strong>way</strong> up if you want lots of layers
even near the starting lake.</figcaption>
</figure>

<h3>Biome Debugging</h3>

<p>Tile placement is based on a range of humidity and 'aux' values
(humidity and aux being properties that vary at different points across the world)
that are suitable for each type of tile.  For example: grass is only placed in
places with relatively high humidity, and desert (not to be confused with plain old sand)
only gets placed where aux is high.  We've taken to calling these constraints 'rectangles',
because when you plot each tile's home turf on a chart of humidity and aux,
they are shown as rectangles.</p>

<p>It's hard to make sense of the rectangles just by looking at the autoplace code
for each tile, so I wrote a script to chart them.  This allowed us to ensure that
they were arranged as we wanted, with no gaps between them,
and with overlap in some cases.</p>

<figure>
<img style="background:white" src="http://fs.marvin.nuke24.net/uri-res/raw/urn:sha1:REEUFB7J5BH4IHIVXDXPUM5PV4FKJ5VV/rects.svg" width="900" height="900"/>
<figcaption>Rectangles.</figcaption>
</figure>

<p>Having the humidity-aux-tile chart is all well and good, but doesn't tell the whole story,
since tile placement also depends on a noise layer specific to each tile type,
and could also influenced by user-adjustable autoplace controls (e.g. turning the grass slider up).
So to further help us visualize how humidity, aux, tile-specific noise, and
autoplace controls worked together to determine tiles on the map,
there are a couple of alternate humidity and aux generators that simply vary them
linearly from north-south and west-east, respectively.</p>

<figure>
<img src="https://eu1.factorio.com/assets/img/blog/fff-282-moisture+aux-debug-map.png" width="900" height="900"/>
<figcaption>Using 'debug-moisture' and 'debug-aux' generators to drive moisture and aux, respectively.</figcaption>
</figure>

<p>This map helped us realize that, rather than having controls
for each different type of tile, it made more sense to just
control moisture and aux (which is called 'terrain type' in the GUI,
because 'aux' doesn't mean anything).</p>

<figure>
<img src="https://eu1.factorio.com/assets/img/blog/fff-282-moisture+aux-controls.gif" width="900" height="664"/>
<figcaption>Sliding the moisture and aux bias sliders to make the world more or less grassy or red-deserty.</figcaption>
</figure>

<p>A pet project of mine has been to
put controls in the map generator GUI so that we could select generators
for various tile properties (temperature, aux, humidity, elevation, etc) at
map-creation time without necessarily needing to involve mods.
This was useful for debugging the biome rectangles, but my ulterior
motive was to, at least in cases where there are multiple options,
show the generator information to players.  A couple of reasons for
this:</p>

<ul>
<li>It was already possible for mods to override tile property generators via presets, but
we didn't have a place to show that information in the UI.
So switching between presets could change hidden variables in non-obvious ways
and lead to a lot of confusion.</li>

<li>I had dreams of shipping alternate elevation generators in the base game.</li>
</ul>


<h3>Water Placement</h3>

<p>For 0.16
I <a href="https://www.factorio.com/blog/post/fff-207">attempted to
make the shape of continents more interesting</a>.

Some people
<a href="https://www.reddit.com/r/factorio/comments/7n3mn1/016_swamplands_are_pure_awesome/">really liked</a> the
<a href="https://www.youtube.com/watch?v=-pkwTjG-sJg">new terrain</a>,
or at least <a href="https://www.reddit.com/r/factorio/comments/7krq86/question_about_water_generation_in_016/">managed to find some settings that made it work</a> for them.
Others called it a "swampy mess".
A common refrain was that the world was more fun to explore in the 0.12 days.</p>

<p>So in 0.17 we're restoring the <em>default</em> elevation generator to one very similar to that used
by 0.12.  Which means large, sometimes-connected lakes.</p>

<p>The water 'frequency' control was confusing to a lot of people including myself.
It could be interpreted as "how much water", when the actual effect was to inversely
scale both bodies of water and continents, such that higher water frequency actually meant smaller bodies of water.
So for 0.17, the water 'frequency' and 'size' sliders are being replaced with 'scale' and 'coverage',
which do the same thing, but in a hopefully more obvious way.
Larger scale means larger land features, while more coverage means more of the map covered in water.</p>


<h3>New Map Types</h3>

<p>In order to ensure a decent starting area, the elevation generator
always makes a plateau there (so you'll never spawn in the middle of
the ocean), and a lake (so you can get a power plant running).
Depending on what's going on outside of that plateau, this sometimes resulted in
a circular ring of cliffs around the starting point,
which looked very artificial, and we wanted to reduce that effect.</p>

<p>In the process of solving that problem I created another custom generator for debugging purposes.
This one simply generated that starting area plateau in an endless ocean.
I don't actually remember how this was useful for debugging, but at one point I directed Twinsen to look at it
to illustrate the mechanics behind generating the starting area.</p>

<p>The rest of the team liked that setting so much that we're making it a player-selectable option.
So in 0.17 you'll get to pick between the 'Normal' map type, which resembles that from 0.12,
and 'Island', which gives you a single large-ish island at the starting point.
Maps with multiple starting points will have multiple islands.
There's a slider to let you change the size of the island(s).</p>

<figure>
<img src="https://eu1.factorio.com/assets/img/blog/fff-282-islands.png" width="900" height="900"/>
<figcaption>PvP islands!</figcaption>
</figure>

<p>And speaking of scale sliders, we're expanding their range from ± a factor of 2 (the old 'very low' to 'very high' settings)
to ± a factor of 6 (shown as '17%' to '600%').
Previously the values were stored internally as one of 5 discrete options,
but as the recent terrain generation changes have made actual numeric multipliers more meaningful in most contexts
(e.g. the number of ore patches is directly proportional to the value of the 'frequency' slider,
rather than being just vaguely related to it somehow),
we're switching to storing them as numbers.
This has the side-effect that if you don't mind
<a href="https://wiki.factorio.com/User:TOGoS/Creating_a_map_from_custom_settings">editing some JSON</a>,
you'll be able to create maps with values outside the range provided by the GUI sliders.</p>

<p>Mods will be able to add their own 'map types' to the map type drop-down, too.</p>

<p>If you really liked the shape of landmasses in 0.16, spam <a href="wherever">the forum</a> about it.</p>

</body>
</html>
