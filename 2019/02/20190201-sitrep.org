#+TITLE: 2019-02-01 Terrain Generation Situation Report

Waiting for water-horse/option-b/whatever branch to get merged.

~c6331149574b9f1f94a3800684657569f6982d68~ - simplify named expressions as they're compiled to better handle constsnts.
This was a prerequisite for ~d5efc137d7b2af397e674822e51a1d6d3d3975a9~, on the ~terrain-generation/20190201-stuff~ branch.

** DONE Cherry-pick c6331149574b9f1f94a3800684657569f6982d68 to master
** DONE Generate a map with TFMPM and see if it sped up compilation or reduced the number of registers or anything

What do I compare it to?

Looks like I did the register allocation stuff [[../../2018/20181227-sitrep.org][back on December 27th]].
The [[http://tfmpm.togos-fbs.nuke24.net/uri-res/raw/urn:sha1:FXWTVLMN2KZGPFHDJTUGY4CKTZQEPUA7][log file for after that change]] says:

#+BEGIN_SRC
   1.632 Verbose CompiledMapGenSettings.cpp:486: MapGenSettings compilation took 0.297770 seconds; deduplicated 162 procedures and 3228 sub-expressions; resulting program has 233 procedures, 7837 operations, and 272 registers
   6.812 Map preview generation time: 5.477005 seconds.
#+END_SRC

[[http://tfmpm.togos-fbs.nuke24.net/uri-res/raw/urn:sha1:MSIYMJM2DLMFIGAJWK3JNQIIC67PFYC7/factorio.log][And now]]...

#+BEGIN_SRC
   1.980 Verbose CompiledMapGenSettings.cpp:486: MapGenSettings compilation took 0.519670 seconds; deduplicated 163 procedures and 3266 sub-expressions; resulting program has 234 procedures, 7819 operations, and 273 registers
   6.241 Map preview generation time: 4.780457 seconds.
#+END_SRC

Oh no, compilation time got way worse!  Probably because that additional simplify is basically a giant copy.

(I did [[http://tfmpm.togos-fbs.nuke24.net/uri-res/raw/urn:sha1:I3HFORNB32MCQ5TCP4ZVBSG6PG6XKZOW][re-run]] it to make sure that high compilation wasn't a fluke.)

So come back to this some other time, maybe.

** CANCEL Run tests

Since it was all slow I'm not bothering.

** People complaining about round or crescent lakes

#+BEGIN_QUOTE
The circular aspects of the starting area are because the noise function we use is unpredictable at small scales (we know what it'll do on average, but not at any given point).  So in some rare cases the noise by itself will not make a lake.  So we take the minimum of the noise-based starting area elevation and that of a conical pit, to make sure there is always some water in the starting area.  A nice solution would be to randomly generate some shape other than a cone with known characteristics (like having at least 1 point below zero).  That could also help solve the problem of circular ore patches and biter bases.
So that's a thing on my to-do list.
the elevation function would then be something like
#+BEGIN_SRC
min(max(global noise-based elevation, random-but-controlled-hill to ensure a place for resources), random-but-controlled starting area lake)
#+END_SRC
#+END_QUOTE

