#+TITLE: 2019-02-15 FFF

- Previous terrain fffs:
  - https://www.factorio.com/blog/post/fff-207
  - https://www.factorio.com/blog/post/fff-219
  - https://www.reddit.com/r/factorio/comments/7n3mn1/016_swamplands_are_pure_awesome/
  - https://www.reddit.com/r/factorio/comments/7krq86/question_about_water_generation_in_016/ - mixed reviews, advice on getting good terrain
  - https://forums.factorio.com/viewtopic.php?f=5&t=27655 - "0.12 worldgen is still the best"
  - KoS and Xterm exploring
    - https://www.youtube.com/watch?v=-pkwTjG-sJg
    - https://www.youtube.com/watch?v=5FvQZOp69eg 
- Biters
  - Size slider didn't used to work
  - GIF
  - Frequency and size now work, and are more independent
    - [[http://picture-files.nuke24.net/uri-res/raw/urn:bitprint:OMIU62FYXZUSI6EUSHMLSO5DGDXYTTJL.HMOFTRGI7ZHRZF42NYHA5TZ254OAJ23Z2TWEOYA/0.17-enemy-base-controls.gif]]
  - Maybe mention the underlying noise functions that mods can mess with
- Water body placement
  - New maps get 0.12-like water
  - Old maps keep their generator
    (but you can jump into the editor and change the generator in map gen settings if you really want to)
  - Also an island mode
- Cliffs
  - Some smoothing applied
  - Controls simplified to 'frequency' and 'continuity'
- Wider range
- Overridable

#+BEGIN_SRC
Be sure to include the tiles svg
and the islands story :slightly_smiling_face: How it started as a debug option (edited) 
#+END_SRC
-- Twinsen

