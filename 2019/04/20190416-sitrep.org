#+TITLE: Terrain Generation Situation Report, 2019-04-16

- Still waiting on go-ahead to merge https://github.com/wube/Factorio/pull/2412
  - Any new changes to the terrain generation are going to be waiting on this
- Fixed that the rail world preset wasn't re-selected when used to generate a map
  (https://trello.com/c/W67AbOsj/801-map-preset-not-re-selected-correctly-in-map-generator-gui),
  though the problem that presets won't be re-selected if anything is changed remains,
  because there's nothing in place to remember what preset was used
  - Twinsen says we should remember the preset that was used
- Something about generating animations that I was working on
  - Oh yeah this included a branch that added an option to multioctave noise
    to use an improved offset algorithm

** global->mapGenSettings horseshit

Where does it even get set?

Map gets created in ~InGenerateMapDialog::process~, which is in ~AppManagerStates.cpp~.

~global->mapGenSettings~ doesn't get set; it gets 'filled'!
Which is why searching for "~global->mapGenSettings =~" was not finding what I wanted.

~global->mapGenSettings~ then gets referenced down in ~ScenarioPrototype::copyFactoryTo~.
