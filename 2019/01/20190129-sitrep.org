#+TITLE: Fixing your bad_weak_ptr errors

Getting ~bad_weak_ptr~ errors when calling shared_from_this

Let's try replacing

#+BEGIN_SRC 
      return std::make_shared<NoiseExpressions::Add>(pTree));
#+END_SRC

with

#+BEGIN_SRC 
      return std::make_shared<NoiseExpression>(new NoiseExpressions::Add(pTree));
#+END_SRC

Oh, that doesn't even compile.  Try again:

#+BEGIN_SRC 
    if (functionName == "add")
      return std::shared_ptr<NoiseExpression>(new NoiseExpressions::Add(pTree));
#+END_SRC

Result: still a ~bad_weak_ptr~:

#+BEGIN_SRC
  25.471 Error NoiseExpressions.cpp:298: Oh no, I did a bad shared_from_this!

{
  "type": "function-application",
  "function_name": "add",
  "arguments": ...
}
#+END_SRC

