#+TITLE: Terrain Generation Situation Report, 2019-03-07..08

- I found [[https://forums.factorio.com/viewforum.php?f=7][where the bug reports are]]!

** DONE Fix "random seed stops working after importing exchange string"

https://forums.factorio.com/65555

Here's a map exchange string:

#+BEGIN_SRC
>>>eNpjYBBk4GAAgwZ7BoYDDhwsyfmJOUCeHUSkwZ4rOb+gILVIN
78odfWqVXBhzuSi0pRU3fxMVMWpeam5lbpJicWpMCEQ5sgsys9DN
4G1uCQ/D1WkpCg1tRhZI3dpUWJeZmkuSC+yPQyMs6aU6Da0yDGA8
P96BoX//0EYyHoA9MsDBga4pxiBYjDAmpyTmZbGwKDgCMROIGlGR
sZqkXXuD6um2DNC1Og5QBkfoCIRu6EiD1qhjIjVUEbHYSjDYT6MU
Q9j9DswGoPBZ3sEA2JXCdBkqCUcDggGRLIFJMnI2Pt264Lvxy7YM
f5Z+fGSb1KCPWOmbKivQOl7O6AkO1ADIxOcmDUTBHbCfMAAM/OBP
VTqpj3j2TMg8MaekRWkQwREOFgAiQPezAyMAnxA1oIeIKEgwwBzm
h3MGBEHxjQw+AbzyWMY47I9uj9UHBhtQIbLgYgTIAJsIdxljFBmp
ANEQhIhC9RqxIBsfQrCcydhNh5GshrNDSowN5g4YPECmogKUsBzg
exJgRMvmOGOAIbgBXYYDxi3zAwIAEwf4kcN8wCo0aMh<<<
#+END_SRC

#+BEGIN_QUOTE
Steps to reproduce:
- Play / New Game
- Expand Preview
- Import map exchange string (input a string then confirm)
- Click "Random seed"
- The number of the seed will change, but the map itself won't update
#+END_QUOTE

I have reproduced it.  The preview regenerates; it just doesn't look any different.
Bodies of water are all the same.

Findings:

- After the randomize button is clicked, the text in the seed field is randomized
  - Then fillSettings gets called by the next getValidSettings().
  - exchangeString is non-empty.  So this block runs:
    #+BEGIN_SRC C++ -l 413
      if (!this->exchangeString.empty())
      {
        try
        {
          MapGenSettings mapGenSettingsAttempt;
          MapSettings mapSettingsAttempt(*global->mapSettings);
          
          ExchangeString::loadMapExchangeString(this->exchangeString, mapGenSettingsAttempt, mapSettingsAttempt);
          
          mapGenSettings = mapGenSettingsAttempt;
          mapSettings = mapSettingsAttempt;
          return true;
        }
        catch (const std::runtime_error&)
        {}
      }
    #+END_SRC
    It returns.  Which means the new seed never gets looked at.  Or any other settings!  Does changing other settings clear the exchange string maybe?
- When I change water coverage...
  - Exchange string gets emptied, apparently, because that block gets skipped.
  - Where does that happen?
  - Putting breakpoint on waterCoverageSlider.value change...
    - Which ends up calling ~MapGeneratorGui::onValueChanged~
      - Which calls ~settingsChanged~
        - Which, as long as ~ignoreChanges~ is false, calls ~exchangeString.clear()~

Fixed by [[https://github.com/Wube/Factorio/commit/cf57b80917be7b46ca2eaa8089c6efe523cc94e9][cf57b80917be7b46ca2eaa8089c6efe523cc94e9]].

** DONE Fix the 3-peak problem

I suspect the problem is line 344 of AutoplaceSpecification.cpp

We do integer division of 2 by the
number of peaks to determine the distance from optimal for that peak.
Which works fine when number of peaks is 1 or 2, but will give zero
for anything larger (e.g. 3).

#+BEGIN_SRC C++ -l 340
      peakInfluence["arguments"].add( // (1 - totalDistance * 2 / peak->dimensions.size());
        NoiseExpressions::subtract(
          NoiseExpressions::literal(1),
          NoiseExpressions::multiply(
            NoiseExpressions::literal(int(2 / peak->dimensions.size()))
            totalDistance)));
#+END_SRC

The corresponding part of the 0.16 code looks like this:

#+BEGIN_SRC
influence *= (1 - 2 * totalDistance / peak->dimensions.size());
#+END_SRC

Note that ~totalDistance~ is a float in that case.

Fixed version:

#+BEGIN_SRC C++ -l 340
      peakInfluence["arguments"].add( // (1 - totalDistance * 2 / peak->dimensions.size());
        NoiseExpressions::subtract(
          NoiseExpressions::literal(1),
          NoiseExpressions::multiply(
            NoiseExpressions::literal(2.0 / peak->dimensions.size()),
            totalDistance)));
#+END_SRC

I think f939fc4f84621cc18f77989336846b18d7af66d4 should solve this, though I HAVEN'T TESTED IT BECAUSE I LACK TEST CASE.

Should I build some kind of framework for testing that autoplace specifications behave the same between 0.16 and 0.17?

** DONE Make normally-hidden noise expressions available in the GUI when loading mapgensettings that reference them

tl;dr: [[https://github.com/Wube/Factorio/commit/08ea1a0c652823ecdef8ffdb006c8d53045e6029][Done and on master]]. [2019-03-08T15:48:40-06:00]

Test cases:
- [ ] Normal new game still works normal
- [ ] 0.16 elevation ("Swamps") available in editor when loading 0.16 map
  - [ ] Loaded map
  - [ ] Exchange string
- [ ] Debug settings available when map created with them
  - [ ] Loaded map
  - [ ] Exchange string
- [X]
  - Load 0.16 map
  - change elevation to normal
  - export exchange string
  - import exchange string
  - 'swamp' option should be gone

0.16 exchange string:

#+BEGIN_SRC
>>>eNpjYBBk4GQAgwZ7EOZhSc5PzIHxQJgrOb+gILVIN78oFVmYM7mo
NCVVNz8TRTFbSmpxalEJshBLSiaqAFdqXmpupW5SYjGKgazpRYnFxcg
iHJlF+Xno9rIUJ+aloOgrLsnPQzWppCg1FcUk7tKixLzM0lx0wxgYp8
89dayhRY4BhP/XMxj8/w/CQNYFYJBcYGCAhw0jUAwKGDlTc1LLEksy8
/P4DOINzXThXNbknMy0NAYGBUcIbrBnZGSsFlnn/rBqij0jRLeeA5Tx
ASoSsRsq8qAVyohYDWV0HIYyHObDGPUwRr8DozEYfLZHMCB2lQBNhlr
C4YBgQCRbQJKMjL1vty74fuyCHeOflR8v+SYl2DNmyob6CpS+twNKso
N8yQQnZs0EgZ0wHzDAzHxgD5W6ac949gwIvLFnZAXpEAERDhZA4oA3M
wOjAB+QtaAHSCjIMMCcZgczRsSBMQ0MvsF88hjGuGyP7g8VB0YbkOFy
IOIEiABbCHcZI5QZ6QCRkETIArUaMSBbn4Lw3EmYjYeRrEZzgwrMDSY
OWLyAJqKCFPBcIHtS4MQLZrgjgCF4gR3GA8YtMwMCANPHn4nfSgCuzb
qu<<<
#+END_SRC

Debug settings (saved as xxx.debugsettings) exchange string
(references named noise expressions that don't exist normally and aren't valid literals):

#+BEGIN_SRC
>>>eNp9Uj1oFEEUnvE8c5dASPAgCl444ToZMXpoinA72qSQlEK00
bnduTC4O3Pszoo/RVJESBGxEUEb04pgZxFshIAoKASt7E4stFBUB
G2Ec+Z2ZndvUR+8xzffN+9vdgGYBlUwtDVHe2WvK4hvT9rHXdHr0
RCJkObpqhvGHkWCjV6mnAbXUIdEI5crLBS8WKEcScFHGRlSGuWZi
TgknMVBMRfAj/vFhbX1WaB9sAoag4F2hfpqlz4A6VJQccb2lUh8t
erRTryCNKI+vUIkE3zm2MW5U4hFPuFedMQVXDJOuYwqgWCRjEM6m
STZYz2SJFR3VpBPLlPEFU0RCXo+k+pRDv9XRfOodSC9URDr/xLQ3
EnUmpA0UJ+C6BGmk4lyTNn1WbcLQOO08jN6dwjhjdrjxffX7zgwe
YGj2IBvhll+apj+TQOWHxmwsWMAfmDBqgW3MTwxtB9OBpJeUlU2T
So4A4m4rkUIb315svXr+W4b/n74/c1S55IDWf3c0lT8ta3EMZUA9
6Th/j1t23YDYGv2HSO9c+DrV9o+O7CsM2o64HkVnp0tATg1qdDWp
gqNQ8CO1rZlahh2h/bTbvLBgrdOcY8mhgu6+KwOL3QYNkwngwaex
4lwMFNV6nGQb+9ly720HXdyrQszNO0MLfyXFQpMM/fw47qPl4ZPp
XQI9YK7Y/akvm0JZKb+j8Xtu/Efec7xhQ==<<<
#+END_SRC

Debug options:
- Noise amplitude options don't show in dropdown
- Hit confirm: "noise expression starting-lake-noise-amplitude-8-4" is not defined
- I guess it didn't get changed because there's no dropdown
  - But then how did the save load at all?
  - Maybe because the save included the compiled mapgensettings, lol.
- So anyway, maybe the MapGeneratorGui can fix this
- This does *not* happen if we get an exchange string and load it!  In that case the map loads fine,
  even though (according to TerrainGeneratorsGuiHelper#fillMapGenSetttings) ~starting-noise-amplitude = starting-noise-amplitude-16-4~
  - It defaulted to that (due to it being the only option in ~noisePropertyOverrideOptions~, and kept it.

Look into:
- [ ] noisePropertyOverrideOptions probably shouldn't include invalid options
- [ ] What's been actually picked when mapgensettings actually get compiled?
  - new map with imported exchange string
  - mapgensettings from editor
- [ ] Does the exchange string even include the invalid settings?
- [ ] Load exchange string containing invalid noise expression names, *don't change any settings*, then start

# To strip out timestamps in logs:
# (replace-regexp "^ [0-9][0-9][0-9]\.[0-9][0-9][0-9] " " ")

*** Debugging, take 1

Loaded and edited (changed Aux to Normal to force recompile):
#+BEGIN_SRC
 Info MapGeneratorGui.cpp:2047: Loaded (by default) propertyExpressionNames into MapGenSettings:
 Info MapGeneratorGui.cpp:2049:   aux = debug-aux
 Info MapGeneratorGui.cpp:2049:   elevation = 0_17-islands+continents
 Info MapGeneratorGui.cpp:2049:   moisture = debug-moisture
 Info MapGeneratorGui.cpp:2049:   starting-lake-noise-amplitude = starting-lake-noise-amplitude-8-4
 Info MapGeneratorGui.cpp:2049:   starting-noise-amplitude = starting-noise-amplitude-16-4
 Info MapGeneratorGui.cpp:2049:   temperature = debug-temperature
 Info MapGeneratorGui.cpp:2058: Loaded propertyExpressionNames into MapGenSettings:
 Info MapGeneratorGui.cpp:2060:   aux = aux
 Info MapGeneratorGui.cpp:2060:   elevation = 0_17-islands+continents
 Info MapGeneratorGui.cpp:2060:   moisture = debug-moisture
 Info MapGeneratorGui.cpp:2060:   starting-lake-noise-amplitude = starting-lake-noise-amplitude-8-4
 Info MapGeneratorGui.cpp:2060:   starting-noise-amplitude = starting-noise-amplitude-16-4
 Info MapGeneratorGui.cpp:2060:   temperature = debug-temperature
 Info CompiledMapGenSettings.cpp:363: CompiledMapGenSettings: using propertyExpressionNames:
 Info CompiledMapGenSettings.cpp:365:   elevation = 0_17-islands+continents
 Info CompiledMapGenSettings.cpp:365:   moisture = debug-moisture
 Info CompiledMapGenSettings.cpp:365:   starting-lake-noise-amplitude = starting-lake-noise-amplitude-8-4
 Info CompiledMapGenSettings.cpp:365:   starting-noise-amplitude = starting-noise-amplitude-16-4
 Info CompiledMapGenSettings.cpp:365:   temperature = debug-temperature
 Error MainLoop.cpp:1129: Exception at tick 717: Noise expression 'starting-lake-noise-amplitude-8-4' is not defined.
#+END_SRC

New game based on exchange string (and changed Aux to Normal):
#+BEGIN_SRC
 Info MapGeneratorGui.cpp:2047: Loaded (by default) propertyExpressionNames into MapGenSettings:
 Info MapGeneratorGui.cpp:2049:   starting-noise-amplitude = starting-noise-amplitude-16-4
 Info MapGeneratorGui.cpp:2058: Loaded propertyExpressionNames into MapGenSettings:
 Info MapGeneratorGui.cpp:2060:   aux = aux
 Info MapGeneratorGui.cpp:2060:   elevation = 0_17-islands+continents
 Info MapGeneratorGui.cpp:2060:   moisture = debug-moisture
 Info MapGeneratorGui.cpp:2060:   starting-noise-amplitude = starting-noise-amplitude-16-4
 Info MapGeneratorGui.cpp:2060:   temperature = debug-temperature
 Info CompiledMapGenSettings.cpp:363: CompiledMapGenSettings: using propertyExpressionNames:
 Info CompiledMapGenSettings.cpp:365:   elevation = 0_17-islands+continents
 Info CompiledMapGenSettings.cpp:365:   moisture = debug-moisture
 Info CompiledMapGenSettings.cpp:365:   starting-noise-amplitude = starting-noise-amplitude-16-4
 Info CompiledMapGenSettings.cpp:365:   temperature = debug-temperature
#+END_SRC

(Timestamps removed for easier reading.)

~starting-noise-amplitude-16-4~ actually is defined; that's why it can be the default.
Let's take that shit out because it's confusing, and try this all again.

*** Debugging, take 2

Loaded and edited (changed Aux to Normal to force recompile):
#+BEGIN_SRC
 Info MapGeneratorGui.cpp:2047: Loaded (by default) propertyExpressionNames into MapGenSettings:
 Info MapGeneratorGui.cpp:2049:   aux = debug-aux
 Info MapGeneratorGui.cpp:2049:   elevation = 0_17-islands+continents
 Info MapGeneratorGui.cpp:2049:   moisture = debug-moisture
 Info MapGeneratorGui.cpp:2049:   starting-lake-noise-amplitude = starting-lake-noise-amplitude-8-4
 Info MapGeneratorGui.cpp:2049:   starting-noise-amplitude = starting-noise-amplitude-16-4
 Info MapGeneratorGui.cpp:2049:   temperature = debug-temperature
 Info MapGeneratorGui.cpp:2058: Loaded propertyExpressionNames into MapGenSettings:
 Info MapGeneratorGui.cpp:2060:   aux = aux
 Info MapGeneratorGui.cpp:2060:   elevation = 0_17-islands+continents
 Info MapGeneratorGui.cpp:2060:   moisture = debug-moisture
 Info MapGeneratorGui.cpp:2060:   starting-lake-noise-amplitude = starting-lake-noise-amplitude-8-4
 Info MapGeneratorGui.cpp:2060:   starting-noise-amplitude = starting-noise-amplitude-16-4
 Info MapGeneratorGui.cpp:2060:   temperature = debug-temperature
 Info CompiledMapGenSettings.cpp:363: CompiledMapGenSettings: using propertyExpressionNames:
 Info CompiledMapGenSettings.cpp:365:   elevation = 0_17-islands+continents
 Info CompiledMapGenSettings.cpp:365:   moisture = debug-moisture
 Info CompiledMapGenSettings.cpp:365:   starting-lake-noise-amplitude = starting-lake-noise-amplitude-8-4
 Info CompiledMapGenSettings.cpp:365:   starting-noise-amplitude = starting-noise-amplitude-16-4
 Info CompiledMapGenSettings.cpp:365:   temperature = debug-temperature
 Error MainLoop.cpp:1129: Exception at tick 12188: Noise expression 'starting-lake-noise-amplitude-8-4' is not defined.
#+END_SRC

New game based on exchange string (and changed Aux to Normal):
#+BEGIN_SRC
 Info MapGeneratorGui.cpp:2047: Loaded (by default) propertyExpressionNames into MapGenSettings:
 Info MapGeneratorGui.cpp:2049:   aux = debug-aux
 Info MapGeneratorGui.cpp:2049:   elevation = 0_17-islands+continents
 Info MapGeneratorGui.cpp:2049:   moisture = debug-moisture
 Info MapGeneratorGui.cpp:2049:   starting-lake-noise-amplitude = starting-lake-noise-amplitude-8-4
 Info MapGeneratorGui.cpp:2049:   starting-noise-amplitude = starting-noise-amplitude-16-4
 Info MapGeneratorGui.cpp:2049:   temperature = debug-temperature
 Info MapGeneratorGui.cpp:2058: Loaded propertyExpressionNames into MapGenSettings:
 Info MapGeneratorGui.cpp:2060:   aux = aux
 Info MapGeneratorGui.cpp:2060:   elevation = 0_17-islands+continents
 Info MapGeneratorGui.cpp:2060:   moisture = debug-moisture
 Info MapGeneratorGui.cpp:2060:   starting-lake-noise-amplitude = starting-lake-noise-amplitude-8-4
 Info MapGeneratorGui.cpp:2060:   starting-noise-amplitude = starting-noise-amplitude-16-4
 Info MapGeneratorGui.cpp:2060:   temperature = debug-temperature
 Info CompiledMapGenSettings.cpp:363: CompiledMapGenSettings: using propertyExpressionNames:
 Info CompiledMapGenSettings.cpp:365:   elevation = 0_17-islands+continents
 Info CompiledMapGenSettings.cpp:365:   moisture = debug-moisture
 Info CompiledMapGenSettings.cpp:365:   starting-lake-noise-amplitude = starting-lake-noise-amplitude-8-4
 Info CompiledMapGenSettings.cpp:365:   starting-noise-amplitude = starting-noise-amplitude-16-4
 Info CompiledMapGenSettings.cpp:365:   temperature = debug-temperature
 Error AppManager.cpp:598: Noise expression 'starting-lake-noise-amplitude-8-4' is not defined.
#+END_SRC

Ah, now they are similar!  Also I recognize now that there are two different noise amplitudes: "starting-lake-noise-amplitude" and "starting-noise-amplitude".

In both cases, the invalid ones are loaded by default, as if ~TerrainGeneratorsGuiHelper::reset~ didn't filter them properly.

*** How did those get in there

#+BEGIN_SRC
 Info MapGeneratorGui.cpp:1966: Apparently debug-aux (referenced by existing settings) is a valid named noise expression; adding to the list of options for aux
 Info MapGeneratorGui.cpp:1966: Apparently 0_17-islands+continents (referenced by existing settings) is a valid named noise expression; adding to the list of options for elevation
 Info MapGeneratorGui.cpp:1966: Apparently debug-moisture (referenced by existing settings) is a valid named noise expression; adding to the list of options for moisture
 Info MapGeneratorGui.cpp:1966: Apparently debug-temperature (referenced by existing settings) is a valid named noise expression; adding to the list of options for temperature
#+END_SRC

No mention of 'starting-lake-noise-amplitude-8-4'.  Add more debug info...

Loading from exchange string is now *not* showing the problem again, which is weird.

From the editor:
#+BEGIN_SRC
 Info MapGeneratorGui.cpp:1955: Adding 0_17-island as an option for elevation based on prototype
 Info MapGeneratorGui.cpp:1955: Adding aux as an option for aux based on prototype
 Info MapGeneratorGui.cpp:1955: Adding elevation as an option for elevation based on prototype
 Info MapGeneratorGui.cpp:1955: Adding moisture as an option for moisture based on prototype
 Info MapGeneratorGui.cpp:1955: Adding temperature as an option for temperature based on prototype
 Info MapGeneratorGui.cpp:1974: TerrainGeneratorsGuiHelper::noisePropertyOverrideOptions:
 Info MapGeneratorGui.cpp:1977:   aux:
 Info MapGeneratorGui.cpp:1979:     aux
 Info MapGeneratorGui.cpp:1977:   elevation:
 Info MapGeneratorGui.cpp:1979:     0_17-island
 Info MapGeneratorGui.cpp:1979:     elevation
 Info MapGeneratorGui.cpp:1977:   moisture:
 Info MapGeneratorGui.cpp:1979:     moisture
 Info MapGeneratorGui.cpp:1977:   temperature:
 Info MapGeneratorGui.cpp:1979:     temperature
 Info MapGeneratorGui.cpp:1955: Adding 0_17-island as an option for elevation based on prototype
 Info MapGeneratorGui.cpp:1955: Adding aux as an option for aux based on prototype
 Info MapGeneratorGui.cpp:1955: Adding elevation as an option for elevation based on prototype
 Info MapGeneratorGui.cpp:1955: Adding moisture as an option for moisture based on prototype
 Info MapGeneratorGui.cpp:1955: Adding temperature as an option for temperature based on prototype
 Info MapGeneratorGui.cpp:1969: Apparently debug-aux (referenced by existing settings) is a valid named noise expression; adding to the list of options for aux
 Info MapGeneratorGui.cpp:1969: Apparently 0_17-islands+continents (referenced by existing settings) is a valid named noise expression; adding to the list of options for elevation
 Info MapGeneratorGui.cpp:1969: Apparently debug-moisture (referenced by existing settings) is a valid named noise expression; adding to the list of options for moisture
 Info MapGeneratorGui.cpp:1969: Apparently debug-temperature (referenced by existing settings) is a valid named noise expression; adding to the list of options for temperature
 Info MapGeneratorGui.cpp:1974: TerrainGeneratorsGuiHelper::noisePropertyOverrideOptions:
 Info MapGeneratorGui.cpp:1977:   aux:
 Info MapGeneratorGui.cpp:1979:     aux
 Info MapGeneratorGui.cpp:1979:     debug-aux
 Info MapGeneratorGui.cpp:1977:   elevation:
 Info MapGeneratorGui.cpp:1979:     0_17-island
 Info MapGeneratorGui.cpp:1979:     elevation
 Info MapGeneratorGui.cpp:1979:     0_17-islands+continents
 Info MapGeneratorGui.cpp:1977:   moisture:
 Info MapGeneratorGui.cpp:1979:     moisture
 Info MapGeneratorGui.cpp:1979:     debug-moisture
 Info MapGeneratorGui.cpp:1977:   temperature:
 Info MapGeneratorGui.cpp:1979:     temperature
 Info MapGeneratorGui.cpp:1979:     debug-temperature
 Info MapGeneratorGui.cpp:2059: Loaded (by default) propertyExpressionNames into MapGenSettings:
 Info MapGeneratorGui.cpp:2070: Loaded propertyExpressionNames into MapGenSettings:
 Info MapGeneratorGui.cpp:2072:   aux = debug-aux
 Info MapGeneratorGui.cpp:2072:   elevation = 0_17-islands+continents
 Info MapGeneratorGui.cpp:2072:   moisture = debug-moisture
 Info MapGeneratorGui.cpp:2072:   temperature = debug-temperature
 Info MapGeneratorGui.cpp:2059: Loaded (by default) propertyExpressionNames into MapGenSettings:
 Info MapGeneratorGui.cpp:2070: Loaded propertyExpressionNames into MapGenSettings:
 Info MapGeneratorGui.cpp:2072:   aux = aux
 Info MapGeneratorGui.cpp:2072:   elevation = 0_17-islands+continents
 Info MapGeneratorGui.cpp:2072:   moisture = debug-moisture
 Info MapGeneratorGui.cpp:2072:   temperature = debug-temperature
 Info MapGeneratorGui.cpp:2059: Loaded (by default) propertyExpressionNames into MapGenSettings:
 Info MapGeneratorGui.cpp:2061:   aux = debug-aux
 Info MapGeneratorGui.cpp:2061:   elevation = 0_17-islands+continents
 Info MapGeneratorGui.cpp:2061:   moisture = debug-moisture
 Info MapGeneratorGui.cpp:2061:   starting-lake-noise-amplitude = starting-lake-noise-amplitude-8-4
 Info MapGeneratorGui.cpp:2061:   starting-noise-amplitude = starting-noise-amplitude-16-4
 Info MapGeneratorGui.cpp:2061:   temperature = debug-temperature
 Info MapGeneratorGui.cpp:2070: Loaded propertyExpressionNames into MapGenSettings:
 Info MapGeneratorGui.cpp:2072:   aux = aux
 Info MapGeneratorGui.cpp:2072:   elevation = 0_17-islands+continents
 Info MapGeneratorGui.cpp:2072:   moisture = debug-moisture
 Info MapGeneratorGui.cpp:2072:   starting-lake-noise-amplitude = starting-lake-noise-amplitude-8-4
 Info MapGeneratorGui.cpp:2072:   starting-noise-amplitude = starting-noise-amplitude-16-4
 Info MapGeneratorGui.cpp:2072:   temperature = debug-temperature
 Info CompiledMapGenSettings.cpp:363: CompiledMapGenSettings: using propertyExpressionNames:
 Info CompiledMapGenSettings.cpp:365:   elevation = 0_17-islands+continents
 Info CompiledMapGenSettings.cpp:365:   moisture = debug-moisture
 Info CompiledMapGenSettings.cpp:365:   starting-lake-noise-amplitude = starting-lake-noise-amplitude-8-4
 Info CompiledMapGenSettings.cpp:365:   starting-noise-amplitude = starting-noise-amplitude-16-4
 Info CompiledMapGenSettings.cpp:365:   temperature = debug-temperature
 Error MainLoop.cpp:1129: Exception at tick 612: Noise expression 'starting-lake-noise-amplitude-8-4' is not defined.
#+END_SRC

What the heck?  It loads okay once, but then fillSettings gets called again?  And is messed up.
*Is it being called again on the existing settings*?

*** Added MapGenSettings#removeInvalidNoiseExpressionReferences

Also:
- Rewrote #normalize to use map.erate(iterator) instead of building a new map
- Moved the code that recognizes literal expressions to NoiseExpressions

This seems to have fixed it.

Log from changing mapgensettings in editor (I changed everything except elevation):

#+BEGIN_SRC
 143.011 Info MapGeneratorGui.cpp:1955: Adding 0_17-island as an option for elevation based on prototype
 143.011 Info MapGeneratorGui.cpp:1955: Adding aux as an option for aux based on prototype
 143.012 Info MapGeneratorGui.cpp:1955: Adding elevation as an option for elevation based on prototype
 143.013 Info MapGeneratorGui.cpp:1955: Adding moisture as an option for moisture based on prototype
 143.014 Info MapGeneratorGui.cpp:1955: Adding temperature as an option for temperature based on prototype
 143.014 Info MapGeneratorGui.cpp:1974: TerrainGeneratorsGuiHelper::noisePropertyOverrideOptions:
 143.015 Info MapGeneratorGui.cpp:1977:   aux:
 143.016 Info MapGeneratorGui.cpp:1979:     aux
 143.016 Info MapGeneratorGui.cpp:1977:   elevation:
 143.017 Info MapGeneratorGui.cpp:1979:     0_17-island
 143.018 Info MapGeneratorGui.cpp:1979:     elevation
 143.018 Info MapGeneratorGui.cpp:1977:   moisture:
 143.019 Info MapGeneratorGui.cpp:1979:     moisture
 143.022 Info MapGeneratorGui.cpp:1977:   temperature:
 143.022 Info MapGeneratorGui.cpp:1979:     temperature
 143.034 Info MapGeneratorGui.cpp:1955: Adding 0_17-island as an option for elevation based on prototype
 143.034 Info MapGeneratorGui.cpp:1955: Adding aux as an option for aux based on prototype
 143.035 Info MapGeneratorGui.cpp:1955: Adding elevation as an option for elevation based on prototype
 143.036 Info MapGeneratorGui.cpp:1955: Adding moisture as an option for moisture based on prototype
 143.036 Info MapGeneratorGui.cpp:1955: Adding temperature as an option for temperature based on prototype
 143.037 Info MapGeneratorGui.cpp:1969: Apparently 0_17-islands+continents (referenced by existing settings) is a valid named noise expression; adding to the list of options for elevation
 143.038 Info MapGeneratorGui.cpp:1969: Apparently debug-moisture (referenced by existing settings) is a valid named noise expression; adding to the list of options for moisture
 143.040 Info MapGeneratorGui.cpp:1969: Apparently debug-temperature (referenced by existing settings) is a valid named noise expression; adding to the list of options for temperature
 143.042 Info MapGeneratorGui.cpp:1974: TerrainGeneratorsGuiHelper::noisePropertyOverrideOptions:
 143.043 Info MapGeneratorGui.cpp:1977:   aux:
 143.043 Info MapGeneratorGui.cpp:1979:     aux
 143.044 Info MapGeneratorGui.cpp:1977:   elevation:
 143.045 Info MapGeneratorGui.cpp:1979:     0_17-island
 143.045 Info MapGeneratorGui.cpp:1979:     elevation
 143.046 Info MapGeneratorGui.cpp:1979:     0_17-islands+continents
 143.047 Info MapGeneratorGui.cpp:1977:   moisture:
 143.047 Info MapGeneratorGui.cpp:1979:     moisture
 143.048 Info MapGeneratorGui.cpp:1979:     debug-moisture
 143.049 Info MapGeneratorGui.cpp:1977:   temperature:
 143.049 Info MapGeneratorGui.cpp:1979:     temperature
 143.051 Info MapGeneratorGui.cpp:1979:     debug-temperature
 143.054 Info MapGeneratorGui.cpp:2059: Loaded (by default) propertyExpressionNames into MapGenSettings:
 143.055 Info MapGeneratorGui.cpp:2061:   aux = aux
 143.055 Info MapGeneratorGui.cpp:2073: Loaded propertyExpressionNames into MapGenSettings:
 143.056 Info MapGeneratorGui.cpp:2075:   elevation = 0_17-islands+continents
 143.057 Info MapGeneratorGui.cpp:2075:   moisture = debug-moisture
 143.057 Info MapGeneratorGui.cpp:2075:   temperature = debug-temperature
 147.592 Info MapGeneratorGui.cpp:2059: Loaded (by default) propertyExpressionNames into MapGenSettings:
 147.593 Info MapGeneratorGui.cpp:2061:   aux = aux
 147.593 Info MapGeneratorGui.cpp:2073: Loaded propertyExpressionNames into MapGenSettings:
 147.594 Info MapGeneratorGui.cpp:2075:   elevation = 0_17-islands+continents
 147.595 Info MapGeneratorGui.cpp:2059: Loaded (by default) propertyExpressionNames into MapGenSettings:
 147.595 Info MapGeneratorGui.cpp:2061:   aux = aux
 147.596 Info MapGeneratorGui.cpp:2061:   elevation = 0_17-islands+continents
 147.597 Info MapGeneratorGui.cpp:2061:   moisture = debug-moisture
 147.598 Info MapGeneratorGui.cpp:2061:   temperature = debug-temperature
 147.598 Info MapGeneratorGui.cpp:2073: Loaded propertyExpressionNames into MapGenSettings:
 147.599 Info MapGeneratorGui.cpp:2075:   elevation = 0_17-islands+continents
 147.608 Info CompiledMapGenSettings.cpp:341: CompiledMapGenSettings: using propertyExpressionNames:
 147.608 Info CompiledMapGenSettings.cpp:343:   elevation = 0_17-islands+continents
#+END_SRC

Loaded from exchange string, changed aux to 'normal':
#+BEGIN_SRC
  83.170 Info MapGeneratorGui.cpp:1955: Adding 0_17-island as an option for elevation based on prototype
  83.170 Info MapGeneratorGui.cpp:1955: Adding aux as an option for aux based on prototype
  83.172 Info MapGeneratorGui.cpp:1955: Adding elevation as an option for elevation based on prototype
  83.173 Info MapGeneratorGui.cpp:1955: Adding moisture as an option for moisture based on prototype
  83.174 Info MapGeneratorGui.cpp:1955: Adding temperature as an option for temperature based on prototype
  83.174 Info MapGeneratorGui.cpp:1969: Apparently debug-aux (referenced by existing settings) is a valid named noise expression; adding to the list of options for aux
  83.176 Info MapGeneratorGui.cpp:1969: Apparently 0_17-islands+continents (referenced by existing settings) is a valid named noise expression; adding to the list of options for elevation
  83.177 Info MapGeneratorGui.cpp:1969: Apparently debug-moisture (referenced by existing settings) is a valid named noise expression; adding to the list of options for moisture
  83.179 Info MapGeneratorGui.cpp:1969: Apparently debug-temperature (referenced by existing settings) is a valid named noise expression; adding to the list of options for temperature
  83.181 Info MapGeneratorGui.cpp:1974: TerrainGeneratorsGuiHelper::noisePropertyOverrideOptions:
  83.182 Info MapGeneratorGui.cpp:1977:   aux:
  83.183 Info MapGeneratorGui.cpp:1979:     aux
  83.183 Info MapGeneratorGui.cpp:1979:     debug-aux
  83.184 Info MapGeneratorGui.cpp:1977:   elevation:
  83.185 Info MapGeneratorGui.cpp:1979:     0_17-island
  83.186 Info MapGeneratorGui.cpp:1979:     elevation
  83.187 Info MapGeneratorGui.cpp:1979:     0_17-islands+continents
  83.188 Info MapGeneratorGui.cpp:1977:   moisture:
  83.188 Info MapGeneratorGui.cpp:1979:     moisture
  83.189 Info MapGeneratorGui.cpp:1979:     debug-moisture
  83.191 Info MapGeneratorGui.cpp:1977:   temperature:
  83.192 Info MapGeneratorGui.cpp:1979:     temperature
  83.193 Info MapGeneratorGui.cpp:1979:     debug-temperature
  86.662 Info CompiledMapGenSettings.cpp:341: CompiledMapGenSettings: using propertyExpressionNames:
  88.399 Info MapGeneratorGui.cpp:2059: Loaded (by default) propertyExpressionNames into MapGenSettings:
  88.399 Info MapGeneratorGui.cpp:2073: Loaded propertyExpressionNames into MapGenSettings:
  88.400 Info MapGeneratorGui.cpp:2075:   elevation = 0_17-islands+continents
  88.401 Info MapGeneratorGui.cpp:2075:   moisture = debug-moisture
  88.401 Info MapGeneratorGui.cpp:2075:   temperature = debug-temperature
  88.408 Info CompiledMapGenSettings.cpp:341: CompiledMapGenSettings: using propertyExpressionNames:
  88.409 Info CompiledMapGenSettings.cpp:343:   elevation = 0_17-islands+continents
  88.410 Info CompiledMapGenSettings.cpp:343:   moisture = debug-moisture
  88.410 Info CompiledMapGenSettings.cpp:343:   temperature = debug-temperature
#+END_SRC

This looks good.  Now to fix MapGenSettings in cases where I don't change anything...

Maybe the place to do it is in MapGenSettings::update.

*** Moved fixing up of propertyExpressionNames to MapGenSettings::update

This seems to work...
- [ ] Editor
- [X] Imported exchange string
- [ ] Loading map from different version I suspect will not work!

but probably makes a lot of the checks that I was adding redundant.  No need for ~removeInvalidNoiseExpressionReferences~.

Another approach to these invalid expression names would be to just ignore them during compilation.
That way if you unloaded a mod, loaded up your save, then reloaded the mod, your old settings would be preserved.
This would also mean the moving around of literal expression parsing is no longer necessary.

*** Need to split this commit up

I'm thinking I should not worry about the invalid settings for now, and just commit the original change,
which is to retain otherwise unavailable settings in the map gen settings GUI.

So the commit with all the stuff I was just working on is 33e40b89b89ee620607dc071a4c2b3330ae03922.

New commits:
- [X] ed91409280 Remove 'starting-noise-amplitude-16-4' expression
- [X] MapGeneratorGui::reset ('rebuild'?) changes that keep existing settings
  - b2abad0caac5c41075c76e69f3d0ae1b134d5da0
- [X] CompiledMapGenSettings should ignore references to nonexistent NamedNoiseExpressions
  - Warn instead of error
  - 462886778cc2dd3dd653a03b23968b3f6eb845a7
  - [/] Note in a comment that this doesn't prevent the problem of an expression referencing a nonexistent one!
    - Whatever
- [X] Recompile when MapDeserialiser#prototypeDataChanged?
  - Making sure it works as I expect:
    - [X] First creating the Scenario = false
    - [X] Loading saved game with same mods = false
    - [ ] Loading saved game after enabling debug expressions
      - Oh no, prototypeDataChanged is still false!
        - "base" mod CRC is 3541142062 ... oh, but the stuff I changed is in 'core'.  Maybe that's not taken into account?
        - Does it change if I change a file in the base mod?
          - Yes, it does!  So actually we're probably good.
      - [X] Also, due to other changes, it should have the funky noise amplitude settings still selected!
  - b1fce8a1cab85bec6d7f365d9273f12dd42d7187
- [X] Allow MapGenSettings to reference nonexistent expressions though, by not filtering them out in the GenerateMapGui for selectors
  (changing namedNoiseExpressions to just a property -> vector<name> map)
  - ACTUALLY THIS COULD CAUSE DESYNCS?
  - Actually no I don't think it could.  Since versions and mods must match exactly,
    the map will be transferred already-compiled to joiners.
  - 0d4dcc57063b5ccc856c26fb36fb96ba1ec61dc2
- [X] Rebase onto master
- [X] Changelog entries:
  - Terrain generator options are preserved by the map generator GUI unless explicitly changed.
  - References to nonexistent noise expressions in map gen settings are ignored instead of crashing the game.
  - Fixed that map generation wouldn't always update to reflect modded noise expressions.
- [X] Maybe rebase again, push

So I think I should keep most of the changes to MapGeneratorGui but toss the other stuff.
- Thouch actually I liked moving the literal expression parsing to somewhere else, so maybe I'll make that its own commit, too.
  - NO DON'T; MAYBE DO IT ON THE EFFICIENCY BRANCH.
    - Okay then nevermind.
** TODO Fix: Default terrain size is limited after playing "Tight spot" campaign

https://forums.factorio.com/66330

[2019-03-08T17:51:00-06:00] Have reproduced.  It does require "restart"ing at least once within tight spot.
- What does "restart" do that a new game doesn't?

#+CAPTION: AppManager.cpp
#+BEGIN_SRC C++ -l 309
  if (global->scenario->getMap() && global->scenario->getMap()->defaultMapGenSettings)
    *global->mapGenSettings = *global->scenario->getMap()->defaultMapGenSettings;
#+END_SRC

Looks like the way ~restartGame()~ works is that it
- creates new map gen settings based on the map's ~defaultMapGenSettings~
  - default as in 'default for a new surface'?
- stores the map gen settings in ~global->mapGenSettings~,
- regenerates the seed
- does a ~this->createGame()~.  I guess settings are passed around implicitly.

