
** Lua stuff

Branch ~terrain-generation/noise-reproduction~ for new features that help me dump
stuff out using Lua.
- Adding ~debug_log_mode~ property (~off~, ~unsigned~, ~signed~)
  and ~next_integer~ to LuaRandomGenerator.cpp

Writing 'reproductions' of RNG and noise in [[./0190305-noise-reproduction.js][0190305-noise-reproduction.js]].

~LuaFluidBox~ has an example of ~luaNewIndex~ that does something.


** Generating test vectors for noise

#+BEGIN_SRC lua
local function generateRngTestVector(seed)
  local rng = game.create_random_generator(seed)
  rng.debug_log_mode = "signed"
  local function rngi() return rng.next_integer() end
  log("Generating results for noise with seed "..seed)
  game.write_file("rng-test-cases.jsonl", game.table_to_json{ seed = seed, mode="float", outputs = { rng(), rng(), rng(), rng(), rng(), rng() } } .. "\n", true)

  rng = game.create_random_generator(seed)
  rng.debug_log_mode = "signed"
  local function rngi() return rng.next_integer() end
  game.write_file("rng-test-cases.jsonl", game.table_to_json{ seed = seed, mode="integer", outputs = { rngi(), rngi(), rngi(), rngi(), rngi(), rngi() } } .. "\n", true)
end

generateRngTestVector(100)
generateRngTestVector(200)
generateRngTestVector(400)
generateRngTestVector(401)
generateRngTestVector(500)
#+END_SRC

Output from Lua dumpy script:

#+BEGIN_SRC
 130.592 Script local function generateRngTestVector(seed)   local rng = game.create_random_generator(seed)   rng.debug_log_mode = "signed"   local function rngi() return rng.next_integer() end   log("Generating results for noise with seed "..seed)   game.write_file("rng-test-cases.jsonl", game.table_to_json{ seed = seed, mode="float", outputs = { rng(), rng(), rng(), rng(), rng(), rng() } } .. "\n", true)    rng = game.create_random_generator(seed)   rng.debug_log_mode = "signed"   local function rngi() return rng.next_integer() end   game.write_file("rng-test-cases.jsonl", game.table_to_json{ seed = seed, mode="integer", outputs = { rngi(), rngi(), rngi(), rngi(), rngi(), rngi() } } .. "\n", true) end  generateRngTestVector(100) generateRngTestVector(200) generateRngTestVector(400) generateRngTestVector(401) generateRngTestVector(500):1: Generating results for noise with seed 100
 130.593 Info RandomGenerator.cpp:90: a:5 b:0 c:1 seed1:1392645 seed2:5376 seed3:44040193 result:45438212
 130.593 Info RandomGenerator.cpp:90: a:5378 b:0 c:193536 seed1:1409307906 seed2:86016 seed3:193536 result:1409544450
 130.594 Info RandomGenerator.cpp:90: a:3028 b:0 c:682 seed1:89140180 seed2:1376256 seed3:-402652502 result:-314234498
 130.594 Info RandomGenerator.cpp:90: a:5 b:0 c:1376258 seed1:45957125 seed2:22020096 seed3:89456642 result:112738311
 130.594 Info RandomGenerator.cpp:90: a:5463 b:2 c:393120 seed1:-738175657 seed2:352321538 seed3:393120 result:-1056593163
 130.594 Info RandomGenerator.cpp:90: a:7125 b:32 c:1344 seed1:89488341 seed2:1342177312 seed3:-12581568 result:-1441431883
 130.602 Info RandomGenerator.cpp:90: a:5 b:0 c:1 seed1:1392645 seed2:5376 seed3:44040193 result:45438212
 130.602 Info RandomGenerator.cpp:90: a:5378 b:0 c:193536 seed1:1409307906 seed2:86016 seed3:193536 result:1409544450
 130.602 Info RandomGenerator.cpp:90: a:3028 b:0 c:682 seed1:89140180 seed2:1376256 seed3:-402652502 result:-314234498
 130.602 Info RandomGenerator.cpp:90: a:5 b:0 c:1376258 seed1:45957125 seed2:22020096 seed3:89456642 result:112738311
 130.604 Info RandomGenerator.cpp:90: a:5463 b:2 c:393120 seed1:-738175657 seed2:352321538 seed3:393120 result:-1056593163
 130.604 Info RandomGenerator.cpp:90: a:7125 b:32 c:1344 seed1:89488341 seed2:1342177312 seed3:-12581568 result:-1441431883
 130.611 Script local function generateRngTestVector(seed)   local rng = game.create_random_generator(seed)   rng.debug_log_mode = "signed"   local function rngi() return rng.next_integer() end   log("Generating results for noise with seed "..seed)   game.write_file("rng-test-cases.jsonl", game.table_to_json{ seed = seed, mode="float", outputs = { rng(), rng(), rng(), rng(), rng(), rng() } } .. "\n", true)    rng = game.create_random_generator(seed)   rng.debug_log_mode = "signed"   local function rngi() return rng.next_integer() end   game.write_file("rng-test-cases.jsonl", game.table_to_json{ seed = seed, mode="integer", outputs = { rngi(), rngi(), rngi(), rngi(), rngi(), rngi() } } .. "\n", true) end  generateRngTestVector(100) generateRngTestVector(200) generateRngTestVector(400) generateRngTestVector(401) generateRngTestVector(500):1: Generating results for noise with seed 200
 130.611 Info RandomGenerator.cpp:90: a:5 b:0 c:1 seed1:1392645 seed2:5376 seed3:44040193 result:45438212
 130.611 Info RandomGenerator.cpp:90: a:5378 b:0 c:193536 seed1:1409307906 seed2:86016 seed3:193536 result:1409544450
 130.611 Info RandomGenerator.cpp:90: a:3028 b:0 c:682 seed1:89140180 seed2:1376256 seed3:-402652502 result:-314234498
 130.611 Info RandomGenerator.cpp:90: a:5 b:0 c:1376258 seed1:45957125 seed2:22020096 seed3:89456642 result:112738311
 130.611 Info RandomGenerator.cpp:90: a:5463 b:2 c:393120 seed1:-738175657 seed2:352321538 seed3:393120 result:-1056593163
 130.611 Info RandomGenerator.cpp:90: a:7125 b:32 c:1344 seed1:89488341 seed2:1342177312 seed3:-12581568 result:-1441431883
 130.619 Info RandomGenerator.cpp:90: a:5 b:0 c:1 seed1:1392645 seed2:5376 seed3:44040193 result:45438212
 130.619 Info RandomGenerator.cpp:90: a:5378 b:0 c:193536 seed1:1409307906 seed2:86016 seed3:193536 result:1409544450
 130.619 Info RandomGenerator.cpp:90: a:3028 b:0 c:682 seed1:89140180 seed2:1376256 seed3:-402652502 result:-314234498
 130.619 Info RandomGenerator.cpp:90: a:5 b:0 c:1376258 seed1:45957125 seed2:22020096 seed3:89456642 result:112738311
 130.620 Info RandomGenerator.cpp:90: a:5463 b:2 c:393120 seed1:-738175657 seed2:352321538 seed3:393120 result:-1056593163
 130.620 Info RandomGenerator.cpp:90: a:7125 b:32 c:1344 seed1:89488341 seed2:1342177312 seed3:-12581568 result:-1441431883
 130.627 Script local function generateRngTestVector(seed)   local rng = game.create_random_generator(seed)   rng.debug_log_mode = "signed"   local function rngi() return rng.next_integer() end   log("Generating results for noise with seed "..seed)   game.write_file("rng-test-cases.jsonl", game.table_to_json{ seed = seed, mode="float", outputs = { rng(), rng(), rng(), rng(), rng(), rng() } } .. "\n", true)    rng = game.create_random_generator(seed)   rng.debug_log_mode = "signed"   local function rngi() return rng.next_integer() end   game.write_file("rng-test-cases.jsonl", game.table_to_json{ seed = seed, mode="integer", outputs = { rngi(), rngi(), rngi(), rngi(), rngi(), rngi() } } .. "\n", true) end  generateRngTestVector(100) generateRngTestVector(200) generateRngTestVector(400) generateRngTestVector(401) generateRngTestVector(500):1: Generating results for noise with seed 400
 130.627 Info RandomGenerator.cpp:90: a:6 b:0 c:1 seed1:1638406 seed2:6400 seed3:52428801 result:54073607
 130.627 Info RandomGenerator.cpp:90: a:1027 b:0 c:214016 seed1:-1879022589 seed2:102400 seed3:214016 result:-1878872061
 130.627 Info RandomGenerator.cpp:90: a:5008 b:0 c:812 seed1:104870800 seed2:1638400 seed3:-2013265108 result:-1906757444
 130.627 Info RandomGenerator.cpp:90: a:6 b:0 c:1638403 seed1:54067206 seed2:26214400 seed3:106496003 result:82837509
 130.627 Info RandomGenerator.cpp:90: a:1127 b:3 c:430624 seed1:-1879022489 seed2:419430403 seed3:430624 result:-1996032444
 130.627 Info RandomGenerator.cpp:90: a:5009 b:62 c:1600 seed1:105280401 seed2:-1879048130 seed3:608175680 result:-1308199441
 130.635 Info RandomGenerator.cpp:90: a:6 b:0 c:1 seed1:1638406 seed2:6400 seed3:52428801 result:54073607
 130.635 Info RandomGenerator.cpp:90: a:1027 b:0 c:214016 seed1:-1879022589 seed2:102400 seed3:214016 result:-1878872061
 130.635 Info RandomGenerator.cpp:90: a:5008 b:0 c:812 seed1:104870800 seed2:1638400 seed3:-2013265108 result:-1906757444
 130.635 Info RandomGenerator.cpp:90: a:6 b:0 c:1638403 seed1:54067206 seed2:26214400 seed3:106496003 result:82837509
 130.635 Info RandomGenerator.cpp:90: a:1127 b:3 c:430624 seed1:-1879022489 seed2:419430403 seed3:430624 result:-1996032444
 130.635 Info RandomGenerator.cpp:90: a:5009 b:62 c:1600 seed1:105280401 seed2:-1879048130 seed3:608175680 result:-1308199441
 130.643 Script local function generateRngTestVector(seed)   local rng = game.create_random_generator(seed)   rng.debug_log_mode = "signed"   local function rngi() return rng.next_integer() end   log("Generating results for noise with seed "..seed)   game.write_file("rng-test-cases.jsonl", game.table_to_json{ seed = seed, mode="float", outputs = { rng(), rng(), rng(), rng(), rng(), rng() } } .. "\n", true)    rng = game.create_random_generator(seed)   rng.debug_log_mode = "signed"   local function rngi() return rng.next_integer() end   game.write_file("rng-test-cases.jsonl", game.table_to_json{ seed = seed, mode="integer", outputs = { rngi(), rngi(), rngi(), rngi(), rngi(), rngi() } } .. "\n", true) end  generateRngTestVector(100) generateRngTestVector(200) generateRngTestVector(400) generateRngTestVector(401) generateRngTestVector(500):1: Generating results for noise with seed 401
 130.643 Info RandomGenerator.cpp:90: a:6 b:0 c:1 seed1:1638406 seed2:6400 seed3:52428801 result:54073607
 130.643 Info RandomGenerator.cpp:90: a:1027 b:0 c:214016 seed1:-1879022589 seed2:102400 seed3:214016 result:-1878872061
 130.646 Info RandomGenerator.cpp:90: a:5008 b:0 c:812 seed1:104870800 seed2:1638400 seed3:-2013265108 result:-1906757444
 130.646 Info RandomGenerator.cpp:90: a:6 b:0 c:1638403 seed1:54067206 seed2:26214400 seed3:106496003 result:82837509
 130.646 Info RandomGenerator.cpp:90: a:1127 b:3 c:430624 seed1:-1879022489 seed2:419430403 seed3:430624 result:-1996032444
 130.646 Info RandomGenerator.cpp:90: a:5009 b:62 c:1600 seed1:105280401 seed2:-1879048130 seed3:608175680 result:-1308199441
 130.653 Info RandomGenerator.cpp:90: a:6 b:0 c:1 seed1:1638406 seed2:6400 seed3:52428801 result:54073607
 130.653 Info RandomGenerator.cpp:90: a:1027 b:0 c:214016 seed1:-1879022589 seed2:102400 seed3:214016 result:-1878872061
 130.653 Info RandomGenerator.cpp:90: a:5008 b:0 c:812 seed1:104870800 seed2:1638400 seed3:-2013265108 result:-1906757444
 130.653 Info RandomGenerator.cpp:90: a:6 b:0 c:1638403 seed1:54067206 seed2:26214400 seed3:106496003 result:82837509
 130.653 Info RandomGenerator.cpp:90: a:1127 b:3 c:430624 seed1:-1879022489 seed2:419430403 seed3:430624 result:-1996032444
 130.654 Info RandomGenerator.cpp:90: a:5009 b:62 c:1600 seed1:105280401 seed2:-1879048130 seed3:608175680 result:-1308199441
 130.661 Script local function generateRngTestVector(seed)   local rng = game.create_random_generator(seed)   rng.debug_log_mode = "signed"   local function rngi() return rng.next_integer() end   log("Generating results for noise with seed "..seed)   game.write_file("rng-test-cases.jsonl", game.table_to_json{ seed = seed, mode="float", outputs = { rng(), rng(), rng(), rng(), rng(), rng() } } .. "\n", true)    rng = game.create_random_generator(seed)   rng.debug_log_mode = "signed"   local function rngi() return rng.next_integer() end   game.write_file("rng-test-cases.jsonl", game.table_to_json{ seed = seed, mode="integer", outputs = { rngi(), rngi(), rngi(), rngi(), rngi(), rngi() } } .. "\n", true) end  generateRngTestVector(100) generateRngTestVector(200) generateRngTestVector(400) generateRngTestVector(401) generateRngTestVector(500):1: Generating results for noise with seed 500
 130.661 Info RandomGenerator.cpp:90: a:7 b:0 c:1 seed1:2048007 seed2:7936 seed3:65011713 result:67067654
 130.661 Info RandomGenerator.cpp:90: a:7427 b:0 c:236544 seed1:-201294589 seed2:126976 seed3:236544 result:-201191165
 130.662 Info RandomGenerator.cpp:90: a:8052 b:0 c:1007 seed1:131088244 seed2:2031616 seed3:939525103 result:1070546075
 130.662 Info RandomGenerator.cpp:90: a:7 b:0 c:2031619 seed1:66535431 seed2:32505856 seed3:132055043 result:98058244
 130.662 Info RandomGenerator.cpp:90: a:7550 b:3 c:468192 seed1:1946189182 seed2:520093699 seed3:468192 result:1795643805
 130.662 Info RandomGenerator.cpp:90: a:3957 b:49 c:1984 seed1:131592053 seed2:-268435407 seed3:1237321664 result:-1105729404
 130.669 Info RandomGenerator.cpp:90: a:7 b:0 c:1 seed1:2048007 seed2:7936 seed3:65011713 result:67067654
 130.669 Info RandomGenerator.cpp:90: a:7427 b:0 c:236544 seed1:-201294589 seed2:126976 seed3:236544 result:-201191165
 130.670 Info RandomGenerator.cpp:90: a:8052 b:0 c:1007 seed1:131088244 seed2:2031616 seed3:939525103 result:1070546075
 130.670 Info RandomGenerator.cpp:90: a:7 b:0 c:2031619 seed1:66535431 seed2:32505856 seed3:132055043 result:98058244
 130.670 Info RandomGenerator.cpp:90: a:7550 b:3 c:468192 seed1:1946189182 seed2:520093699 seed3:468192 result:1795643805
 130.670 Info RandomGenerator.cpp:90: a:3957 b:49 c:1984 seed1:131592053 seed2:-268435407 seed3:1237321664 result:-1105729404
#+END_SRC

C:

#+BEGIN_SRC
1392645 ^ 5376 ^ 44040193 = 45438212
#+END_SRC

Node XOR:

#+BEGIN_SRC
> 1392645 ^ 5376 ^ 44040193
45438212
#+END_SRC

Ah, so, why is it coming out 11883780?

- [ ] Test vectors for RNG
- [ ] Specification of RNG
- [ ] Specification of basis noise
- [ ] Specification of multioctave noise
- [ ] Test vectors for simple noise expressions
