const readline = require('readline');
let rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
	terminal: false,
});

let currentBatch = undefined;
let currentOutputScale = 1;
// InputScale0 is taken into account by x, y.
// InputScale1 is not, so we need to remember and divide by it.
let currentInputScale = 1;
let batches = [];

rl.on('line', (line) => {
	let m;
	if( (m = /MultioctaveNoiseTestParams: x0: ([+-]?\d+\.\d+), y0: ([+-]?\d+\.\d+), seed0: (\d+), seed1: (\d+), width: (\d+), height: (\d+), is0: ([+-]?\d+\.\d+), is1: ([+-]?\d+\.\d+), os0: ([+-]?\d+\.\d+), os1: ([+-]?\d+\.\d+), octaves: ([+-]?\d+\.\d+), persistence: ([+-]?\d+\.\d+)/.exec(line)) ) {
		currentOutputScale = +m[9] * +m[10];
		currentInputScale = +m[8] * 0.5; // The tests multiply inputScale1 by 0.5 to match CachedNoise behavior
		currentBatch = {
			"functionName": "FactorioMultioctaveNoise",
			"seed0": +m[3],
			"seed1": +m[4],
			"octaveCount": +m[11],
			"persistence": +m[12],
			//"inputScale0": +m[7],
			//"inputScale1": +m[8],
			"samples": [],
		}
		batches.push(currentBatch);
	} else if( (m = /\bNoiseTestParams: x0: ([+-]?\d+\.\d+), y0: ([+-]?\d+\.\d+), seed0: (\d+), seed1: (\d+), width: (\d+), height: (\d+), is0: ([+-]?\d+\.\d+), is1: ([+-]?\d+\.\d+), os0: ([+-]?\d+\.\d+), os1: ([+-]?\d+\.\d+)/.exec(line)) ) {
		currentOutputScale = +m[9] * +m[10];
		currentInputScale = +m[8];
		currentBatch = {
			"functionName": "FactorioBasisNoise",
			"seed0": +m[3],
			"seed1": +m[4],
			//"inputScale0": +m[7],
			//"inputScale1": +m[8],
			"samples": [],
		}
		batches.push(currentBatch);
	} else if( (m = /x:([+-]?\d+\.\d+)\s+y:([+-]?\d+\.\d+)\s+value:([+-]?\d+\.\d+)/.exec(line)) ) {
		currentBatch.samples.push({x:+m[1]*currentInputScale, y:+m[2]*currentInputScale, value:+m[3]/currentOutputScale});
	}
});
rl.on('close', () => {
	console.log(JSON.stringify(batches, null, "\t"));
});
